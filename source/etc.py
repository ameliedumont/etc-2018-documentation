from flat import *
from random import *
import codecs

def make_index(lines, dict, folio):
    entrees = [
        "/ETC",
        "adapter",
        "algorithm",
        "Android",
        "art ",
        "artist",
        "Bluetooth",
        "browser",
        "code",
        "code of conduct",
        "code of conflict",
        "collective",
        "communication",
        "community",
        "computer",
        "connection",
        "contributors",
        "control",
        "CSS",
        "data",
        "Debian",
        "decrypt",
        "device",
        "DIY",
        "Docker",
        "documentation",
        "email",
        "encryption",
        "engineering",
        "exchange",
        "Facebook",
        "Fedora",
        "feminine",
        "feminism",
        "feminist",
        "free software",
        "freedom",
        "FTP",
        "fundings",
        "gay",
        "gender",
        "Git",
        "Github",
        "Gitlab",
        "Gnome",
        "Google",
        "Grindr",
        "hardware",
        "hash",
        "HTML",
        "information",
        "infrastructure",
        "install",
        "interface",
        "Internet",
        "IP",
        "IPSec",
        "Javascript",
        "link",
        "Linux",
        "MAC address",
        "Mac OS",
        "machine",
        "male",
        "man",
        "masculine",
        "media",
        "men",
        "network",
        "normativity",
        "NSA",
        "offline",
        "online",
        "open source",
        "OpenVPN",
        "PGP",
        "privacy",
        "private key",
        "programmer",
        "programming",
        "public key",
        "Python",
        "Qubes",
        "queer",
        "Red Hat",
        "secure",
        "security",
        "server",
        "sexist",
        "sex work",
        "sex worker",
        "signature",
        "social network",
        "society",
        "SSH",
        "sustainability",
        "sustainable",
        "Systerserver",
        "technologies",
        "technology",
        "Thunderbird",
        "tunnel",
        "Ubuntu",
        "Unix",
        "user",
        "VPN",
        "web",
        "web inspector",
        "web page",
        "Windows",
        "wireless",
        "woman",
        "women"
    ]
    for line in range(len(lines)):
        #s_line = list(line)
        for i in entrees:
            j = lines[line].find(i)
            if j != -1:
                if folio not in dict[i]:
                    dict[i].append(folio)


def make_name_index(lines, dict, folio):
    for line in lines:
        s_line = list(line)
        character = 0
        for letter in s_line:
            letter_index = s_line.index(letter)
            if letter == "B" and s_line[letter_index + 1] == "e" and s_line[letter_index + 2] == "a" and s_line[letter_index + 3] == "u" and s_line[letter_index + 4] == "v" and s_line[letter_index + 5] == "o" and s_line[letter_index + 6] == "i" and s_line[letter_index + 7] == "r":
                if folio not in dict["de Beauvoir, Simone"]:
                    dict["de Beauvoir, Simone"].append(folio)
            elif letter == "C" and s_line[letter_index + 1] == "a" and s_line[letter_index + 2] == "c" and s_line[letter_index + 3] == "h" and s_line[letter_index + 4] == "e" and s_line[letter_index + 5] == "r" and s_line[letter_index + 6] == "e" and s_line[letter_index + 7] == "s":
                if folio not in dict["Cacheres, Bertha"]:
                    dict["Cacheres, Bertha"].append(folio)
            elif letter == "D" and s_line[letter_index + 1] == "a" and s_line[letter_index + 2] == "v" and s_line[letter_index + 3] == "i" and s_line[letter_index + 4] == "s":
                if folio not in dict["Davis, Angela"]:
                    dict["Davis, Angela"].append(folio)
            elif letter == "H" and s_line[letter_index + 1] == "a" and s_line[letter_index + 2] == "m" and s_line[letter_index + 3] == "i" and s_line[letter_index + 4] == "l" and s_line[letter_index + 5] == "t" and s_line[letter_index + 6] == "o" and s_line[letter_index + 7] == "n":
                if folio not in dict["Hamilton, Margaret"]:
                    dict["Hamilton, Margaret"].append(folio)
            elif letter == "H" and s_line[letter_index + 1] == "o" and s_line[letter_index + 2] == "p" and s_line[letter_index + 3] == "p" and s_line[letter_index + 4] == "e" and s_line[letter_index + 5] == "r":
                if folio not in dict["Hopper, Grace"]:
                    dict["Hopper, Grace"].append(folio)
            elif letter == "K" and s_line[letter_index + 1] == "r" and s_line[letter_index + 2] == "a" and s_line[letter_index + 3] == "s" and s_line[letter_index + 4] == "m" and s_line[letter_index + 5] == "o" and s_line[letter_index + 6] == "g" and s_line[letter_index + 7] == "o":
                if folio not in dict["Krasmogonowi, Hilde"]:
                    dict["Krasmogonowi, Hilde"].append(folio)
            elif letter == "L" and s_line[letter_index + 1] == "a" and s_line[letter_index + 2] == "m" and s_line[letter_index + 3] == "a" and s_line[letter_index + 4] == "r" and s_line[letter_index + 5] == "r":
                if folio not in dict["Lamarr, Hedy"]:
                    dict["Lamarr, Hedy"].append(folio)
            elif letter == "L" and s_line[letter_index + 1] == "o" and s_line[letter_index + 2] == "v" and s_line[letter_index + 3] == "e" and s_line[letter_index + 4] == "l" and s_line[letter_index + 5] == "a" and s_line[letter_index + 6] == "c" and s_line[letter_index + 7] == "e":
                if folio not in dict["Lovelace, Ada"]:
                    dict["Lovelace, Ada"].append(folio)
            elif letter == "P" and s_line[letter_index + 1] == "a" and s_line[letter_index + 2] == "n" and s_line[letter_index + 3] == "k" and s_line[letter_index + 4] == "h" and s_line[letter_index + 5] == "u" and s_line[letter_index + 6] == "r" and s_line[letter_index + 7] == "s" and s_line[letter_index + 8] == "t":
                if folio not in dict["Pankhurst, Sylvia"]:
                    dict["Pankhurst, Sylvia"].append(folio)
            elif letter == "S" and s_line[letter_index + 1] == "n" and s_line[letter_index + 2] == "e" and s_line[letter_index + 3] == "l" and s_line[letter_index + 4] == "t" and s_line[letter_index + 5] == "i" and s_line[letter_index + 6] == "n":
                if folio not in dict["Snelting, Femke"]:
                    dict["Snelting, Femke"].append(folio)
            elif letter == "S" and s_line[letter_index + 1] == "n" and s_line[letter_index + 2] == "o" and s_line[letter_index + 3] == "w" and s_line[letter_index + 4] == "d" and s_line[letter_index + 5] == "e" and s_line[letter_index + 6] == "n":
                if folio not in dict["Snowden, Edward"]:
                    dict["Snowden, Edward"].append(folio)
            elif letter == "S" and s_line[letter_index + 1] == "t" and s_line[letter_index + 2] == "a" and s_line[letter_index + 3] == "l" and s_line[letter_index + 4] == "l" and s_line[letter_index + 5] == "m" and s_line[letter_index + 6] == "a" and s_line[letter_index + 7] == "n":
                if folio not in dict["Stallman, Richard"]:
                    dict["Stallman, Richard"].append(folio)
            elif letter == "T" and s_line[letter_index + 1] == "o" and s_line[letter_index + 2] == "r" and s_line[letter_index + 3] == "v" and s_line[letter_index + 4] == "a" and s_line[letter_index + 5] == "l" and s_line[letter_index + 6] == "d":
                if folio not in dict["Torvald, Linus"]:
                    dict["Torvald, Linus"].append(folio)
            character += 1

def search_categoriesG(lines):
    cat_to_display = {}
    n_line = 0
    for line in lines:
        s_line = list(line)
        categories = ['µ', '~', '#', '>', '`', '^', '¤', '£']
        for letter in s_line:
            if letter in categories:
                if letter == 'µ':
                    if 'Hardware' not in cat_to_display:
                        cat_to_display['Hardware'] = n_line
                elif letter ==  '~':
                    if 'Security/Ecryption' not in cat_to_display:
                        cat_to_display['Security/Encryption'] = n_line
                elif letter ==  '#':
                    if 'Programming' not in cat_to_display:
                        cat_to_display['Programming'] = n_line
                elif letter ==  '>':
                    if 'Free & Open source' not in cat_to_display:
                        cat_to_display['Free & Open source'] = n_line
                elif letter ==  '`':
                    if 'Creativity/Arts' not in cat_to_display:
                        cat_to_display['Creativity/Arts'] = n_line
                elif letter ==  '^':
                    if 'Feminism' not in cat_to_display:
                        cat_to_display['Feminism'] = n_line
                elif letter ==  '¤':
                    if 'DIY' not in cat_to_display:
                        cat_to_display['DIY'] = n_line
                elif letter ==  '£':
                    if 'Games' not in cat_to_display:
                        cat_to_display['Games'] = n_line
        n_line += 1
    return cat_to_display

def search_categoriesD(lines):
    cat_to_display = {}
    n_line = 0
    for line in lines:
        s_line = list(line)
        categories = ['µ', '~', '#', '>', '`', '^', '¤', '£']
        for letter in s_line:
            if letter in categories:
                if letter == 'µ':
                    if 'Hardware' not in cat_to_display:
                        cat_to_display['Hardware'] = n_line
                elif letter ==  '~':
                    if 'Security/Encryption' not in cat_to_display:
                        cat_to_display['Security/Encryption'] = n_line
                elif letter ==  '#':
                    if 'Programming' not in cat_to_display:
                        cat_to_display['Programming'] = n_line
                elif letter ==  '>':
                    if 'Free & Open source' not in cat_to_display:
                        cat_to_display['Free & Open source'] = n_line
                elif letter ==  '`':
                    if 'Creativity/Arts' not in cat_to_display:
                        cat_to_display['Creativity/Arts'] = n_line
                elif letter ==  '^':
                    if 'Feminism' not in cat_to_display:
                        cat_to_display['Feminism'] = n_line
                elif letter ==  '¤':
                    if 'DIY' not in cat_to_display:
                        cat_to_display['DIY'] = n_line
                elif letter ==  '£':
                    if 'Games' not in cat_to_display:
                        cat_to_display['Games'] = n_line
        n_line += 1
    return cat_to_display


def place_categories(n_folio, bloc, bloc2, ligne, col):
    y_p1 = 95.5
    y_p = 4.6
    if n_folio % 2 == 0:
        if col == "col1":
            x_note = 6
            y_note = y_p
            for i in range(0, 31):
                if ligne == i:
                    bloc.frame(x_note, y_note, 55, 50)
                    bloc2.frame(x_note, y_note+2, 25, 50)
                y_note += 5.3

        elif col == "colP1":
            x_note = 6
            y_note = y_p1
            for i in range(0, 13):
                if ligne == i:
                    bloc.frame(x_note, y_note, 55, 50)
                    bloc2.frame(x_note, y_note+2, 25, 50)
                y_note += 5.3
        elif col == "colP2":
            x_note = 64
            y_note = y_p1
            for i in range(0, 13):
                if ligne == i:
                    bloc.frame(x_note, y_note, 55, 50)
                    bloc2.frame(x_note, y_note+2, 25, 50)
                y_note += 5.3
        else:
            x_note = 64
            y_note = y_p
            for i in range(0, 31):
                if ligne == i:
                    bloc.frame(x_note, y_note, 55, 50)
                    bloc2.frame(x_note, y_note+2, 25, 50)
                y_note += 5.3
    else:
        if col == "col1":
            x_note = 9
            y_note = y_p
            for i in range(0, 31):
                if ligne == i:
                    bloc.frame(x_note, y_note, 55, 50)
                    bloc2.frame(x_note, y_note+2, 25, 50)
                y_note += 5.3
        elif col == "colP1":
            x_note = 9
            y_note = y_p1
            for i in range(0, 13):
                if ligne == i:
                    bloc.frame(x_note, y_note, 55, 50)
                    bloc2.frame(x_note, y_note+2, 25, 50)
                y_note += 5.3
        elif col == "colP2":
            x_note = 67
            y_note = y_p1
            for i in range(0, 13):
                if ligne == i:
                    bloc.frame(x_note, y_note, 55, 50)
                    bloc2.frame(x_note, y_note+2, 25, 50)
                y_note += 5.3
        else:
            x_note = 67
            y_note = y_p
            for i in range(0, 31):
                if ligne == i:
                    bloc.frame(x_note, y_note, 55, 50)
                    bloc2.frame(x_note, y_note+2, 25, 50)
                y_note += 5.3



def layout():
    folio = 1
    pages = []

    arts = []
    feminism = []
    open_source = []
    games = []
    security = []
    programming = []
    diy = []
    hardware = []

    index_entries = {
        "/ETC" : [],
        "adapter" : [],
        "algorithm" : [],
        "Android" : [],
        "art " : [],
        "artist" : [],
        "Bluetooth" : [],
        "browser" : [],
        "code" : [],
        "code of conduct" : [],
        "code of conflict" : [],
        "collective" : [],
        "communication" : [],
        "community" : [],
        "computer" : [],
        "connection" : [],
        "contributors" : [],
        "control" : [],
        "CSS" : [],
        "data" : [],
        "Debian" : [],
        "decrypt" : [],
        "device" : [],
        "DIY" : [],
        "Docker" : [],
        "documentation" : [],
        "email" : [],
        "encryption" : [],
        "engineering" : [],
        "exchange" : [],
        "Facebook" : [],
        "Fedora" : [],
        "feminine" : [],
        "feminism" : [],
        "feminist" : [],
        "free software" : [],
        "freedom" : [],
        "FTP" : [],
        "fundings" : [],
        "gay" : [],
        "gender" : [],
        "Git" : [],
        "Github" : [],
        "Gitlab" : [],
        "Gnome" : [],
        "Google" : [],
        "Grindr" : [],
        "hardware" : [],
        "hash" : [],
        "HTML" : [],
        "information" : [],
        "infrastructure" : [],
        "install" : [],
        "interface" : [],
        "Internet" : [],
        "IP" : [],
        "IPSec" : [],
        "Javascript" : [],
        "link" : [],
        "Linux" : [],
        "MAC address" : [],
        "Mac OS" : [],
        "machine" : [],
        "male" : [],
        "man" : [],
        "masculine" : [],
        "media" : [],
        "men" : [],
        "network" : [],
        "normativity" : [],
        "NSA" : [],
        "offline" : [],
        "online" : [],
        "open source" : [],
        "OpenVPN" : [],
        "PGP" : [],
        "privacy" : [],
        "private key" : [],
        "programmer" : [],
        "programming" : [],
        "public key" : [],
        "Python" : [],
        "Qubes" : [],
        "queer" : [],
        "Red Hat" : [],
        "secure" : [],
        "security" : [],
        "server" : [],
        "sexist" : [],
        "sex work" : [],
        "sex worker" : [],
        "signature" : [],
        "social network" : [],
        "society" : [],
        "SSH" : [],
        "sustainability" : [],
        "sustainable" : [],
        "Systerserver" : [],
        "technologies" : [],
        "technology" : [],
        "Thunderbird" : [],
        "tunnel" : [],
        "Ubuntu" : [],
        "Unix" : [],
        "user" : [],
        "VPN" : [],
        "web" : [],
        "web inspector" : [],
        "web page" : [],
        "Windows" : [],
        "wireless" : [],
        "woman" : [],
        "women" : []
    }

    index_names = {
        "de Beauvoir, Simone" : [],
        "Cacheres, Bertha" : [],
        "Davis, Angela" : [],
        "Hamilton, Margaret" : [],
        "Hopper, Grace" : [],
        "Krasmogonowi, Hilde" : [],
        "Lamarr, Hedy" : [],
        "Lovelace, Ada" : [],
        "Pankhurst, Sylvia" : [],
        "Snelting, Femke" : [],
        "Snowden, Edward" : [],
        "Stallman, Richard" : [],
        "Torvald, Linus" : []
    }

    articles = [
        {'titre': 'Hardware \ncrash \ncourse ', 'titreS': 'Hardware crash course ', 'auteur': 'Donna', 'imgFront': 'hcc01.jpg', 'imgIn': 'hcc02.jpg', 'contenu': ''},
        {'titre': 'Tunnel up, \ntunnel down', 'titreS': 'Tunnel up, tunnel down ', 'auteur': 'Mara Elenis Daughter', 'imgIn': 'diffie_hellman.jpg', 'contenu': ''},
        {'titre': 'You are \nokay.', 'titreS': 'You are okay. ', 'auteur': 'Ulrike', 'contenu': ''},
        {'titre': 'Open source \nsustainability', 'titreS': 'Open source sustainability ', 'auteur': 'Helen', 'imgIn': 'upsatge.jpg', 'contenu': ''},
        {'titre': 'A brief \nhistory \nof Linux', 'titreS': 'A brief history of Linux ', 'auteur': 'Aileen', 'imgIn': 'aileen_linux.JPG', 'contenu': ''},
        {'titre': 'Workshop \non Immigration', 'titreS': 'Workshop on Immigration ', 'auteur': 'Milena', 'contenu': ''},
        {'titre': 'Ricette \ndal caos * \nassorbenti \nDIY', 'titreS': 'Ricette dal caos * assorbenti DIY ', 'auteur': 'Lara e Kate', 'contenu': ''},
        {'titre': 'Workshop \non Consent', 'titreS': 'Workshop on Consent ', 'auteur': 'Mujeres Libres Bologna', 'contenu': ''},
        {'titre': 'Imaginary \nfriends', 'titreS': 'Imaginary friends ', 'auteur': 'Obaz', 'imgIn': 'qubes.jpg', 'contenu': ''},
        {'titre': 'Introducing \nthe Soleus \nVPS collective', 'titreS': 'Introducing the Soleus VPS collective ', 'auteur': 'Donna', 'contenu': ''},
        {'titre': 'Build and \nreach the \nInternet with \nan antenna \nfrom the roof', 'titreS': 'Build and reach the Internet with an antenna from the roof ', 'auteur': 'Ignifugo', 'imgIn': 'ninux.jpg', 'contenu': ''},
        {'titre': 'From data \nto dating', 'titreS': 'From data to dating ', 'auteur': 'Frocetaria', 'contenu': ''},
        {'titre': 'Create a \nwebsite that \ncannot be \nhacked but \nlooks awesome!', 'titreS': 'Create a website that cannot be hacked but looks awesome! ', 'imgFin': 'etc_site.jpg', 'auteur': 'Obaz', 'contenu': ''},
        {'titre': 'Dock, dock, \ndocker', 'titreS': 'Dock, Dock, Docker ', 'auteur': 'Ivana', 'imgIn': 'docker.jpg', 'contenu': ''},
        {'titre': 'Presentazione \nrivista \nAspirina', 'titreS': 'Presentazione rivista Aspirina ', 'imgIn': 'aspirina.jpg', 'auteur': '', 'contenu': ''},
        {'titre': 'Inter/de-\npend-ence: \nthe game', 'titreS': 'Inter/de-pend-ence: the game ', 'auteur': 'Sarrita', 'imgFront': 'itg.jpg', 'imgIn': 'interdependence.jpg', 'contenu': ''},
        {'titre': 'F12 how to \nuse a web \ninspector', 'titreS': 'F12 how to use a web inspector ', 'imgFin': 'inspector.jpg', 'auteur': 'Ignifugo', 'contenu': ''},
        {'titre': 'React \nworkshop', 'titreS': 'React workshop ', 'auteur': 'Andrea', 'contenu': ''},
        {'titre': 'Elements for \na feminist \nvideo game \nGodot Engine', 'titreS': 'Elements for a feminist video game Godot Engine ', 'auteur': 'Natacha', 'imgIn': 'jeuvidea.jpg', 'contenu': ''},
        {'titre': 'A Brief \nHistory \nof feminism \nand technology', 'titreS': 'A Brief History of feminism and technology ', 'auteur': 'Zeyev', 'contenu': ''},
        {'titre': 'Sex work \nonline', 'titreS': 'Sex work online ', 'auteur': 'Tiz', 'contenu': ''},
        {'titre': 'Animata: \nan uncommon \nfree software \napproach \nto mapped \nanti-visuals', 'titreS': 'Animata: an uncommon free software approach to mapped anti-visuals ', 'auteur': '', 'contenu': ''},
        {'titre': 'Git explained \nwith toys', 'titreS': 'Git explained with toys ', 'auteur': 'Ignifugo + Obaz', 'contenu': ''}
    ]

    fichiers = [
        'songe.txt',
        'mara.txt',
        'ulrike.txt',
        'helen.txt',
        'aileen.txt',
        'milena.txt',
        'laraekate.txt',
        'mlb.txt',
        'obaz1.txt',
        'donna.txt',
        'ignifugo.txt',
        'frocetaria.txt',
        'obaz2.txt',
        'ivana.txt',
        'aspirina.txt',
        'sarrita.txt',
        'ignifugo2.txt',
        'andrea.txt',
        'natacha.txt',
        'zeyev.txt',
        'tiz.txt',
        'animata.txt',
        'obaz3.txt'
    ]

    purple = rgb(204, 0, 255)
    green = gray(200)


    i=0
    for file in fichiers:

        f = codecs.open(file, "r", "utf-8")
        lignes_brutes = f.readlines()
        f.close()
        lignes = []
        for l in lignes_brutes:
            lignes.append(l.replace("\n", "").replace("/"," /".replace(";", " ;")))
        articles[i]['contenu'] = lignes
        i += 1


    fonte = font.open('Redaction35-Regular.ttf')
    fonte_1 = font.open('CirrusCumulus.otf')
    fonte_folio = font.open('LabMono-Regular.otf')
    fonte_notes = font.open('Redaction35-Regular.ttf')

    white = cmyk(0, 0, 0, 0)
    black1 = cmyk(0, 0, 0, 255)
    black = overprint(black1)

    name = strike(fonte_notes).size(20, 40)
    body = strike(fonte_notes).size(10, 15)
    headline = strike(fonte_1).size(44, 44)
    folios = strike(fonte_folio).size(20, 10)
    note = strike(fonte_folio).size(5.5, 14).color(green)
    legende = strike(fonte_folio).size(7, 10)
    index_note = strike(fonte_notes).size(9, 14)
    index_titre = strike(fonte_notes).size(11, 12)
    index_auteurs = strike(fonte_notes).size(11, 12)
    sommaire = strike(fonte_1).size(18, 10)
    titre_courant = strike(fonte_1).size(14, 14)


    #t = shape().stroke(black).width(1)

    doc = document(130, 180, 'mm')

    p_garde = doc.addpage()
    accueil = "Eclectic Tech Carnival 2018 \n@ XM24 \nBologna, Italy"
    accueil_style = strike(fonte_1).size(18, 20)
    accueil_txt = p_garde.place(accueil_style.text(accueil))
    accueil_txt.frame(12, 7, 90, 153)
    folio += 1
    pages.append(folio)



    p_img = doc.addpage()
    xm24 = image.open('cyborg.jpg')
    xm = p_img.place(xm24)
    xm.frame(8,35,110,160)
    xm.fitwidth(110)
    folio += 1


    intro = "The Eclectic Tech Carnival (/ETC) is a gathering of people who critically use/study/share/ improve everyday information technologies in the context of the free speech and open hardware movements. \nWe are a collective body of feminists with a particular history, who chew on the roots \nof control and domination. \nEvents are organised locally and autonomously where there is a desire to host an /ETC. \nEveryone participates in skill sharing to expand our knowledges."

    introd = strike(fonte_1).size(20, 24)
    p_intro = doc.addpage()
    intro_txt = p_intro.place(introd.text(intro))
    intro_txt.frame(12, 7, 105, 153)
    intro_lines =  intro_txt.frame(12, 7, 105, 153).lines()
    make_index(intro_lines, index_entries, folio)

    folio += 1
    pages.append(folio)


    p_sommaire1 = doc.addpage()
    somm = 'summary'
    somm_txt = p_sommaire1.place(sommaire.text(somm))
    somm_txt.frame(18,5,80,20)
    folio += 1
    p_sommaire = doc.addpage()
    folio += 1
    pages.append(folio)


    f = shape().fill(black)
    t = shape().stroke(green).width(0.1, units='pt')
    t2 = shape().fill(green).stroke(white).width(0.1, units='pt')

    #intro_txt = p_intro.place(introd.text(intro))
    #intro_txt.frame(12, 5, 90, 153)
    p_inter = doc.addpage()
    folio += 1
    p_sommaire2 = doc.addpage()
    #feminism


    x_pg = 25
    x_pg_col1 = 12
    x_pg_col2 = 70
    x_pd = 12
    x_pd_col1 = 15
    x_pd_col2 = 73
    y_bloc1 = 96
    y_bloc_standard = 5
    w_blocs = 49
    h_bloc1 = 65
    h_bloc_standard = 155

    folio += 1
    pages.append(folio)

    cat_page = []

    for a in articles:
        a['p_debut'] = folio

        if 'imgFront' in a:
            p = doc.addpage()
            img = image.open(a.get('imgFront'))
            if folio % 2 == 0:
                block_img = p.place(img)
                block_img.frame(-5,-5,142,170)
                block_img.fitwidth(142)
            folio += 1
            pages.append(folio)
         #si pas imgFront
        page = doc.addpage()
        pieces = [body.paragraph('')]
        pieces.extend(body.paragraph(p) for p in a.get('contenu'))
        auteur = page.place(name.text(a.get('auteur')))
        titre = page.place(headline.text(a.get('titre')))

        if folio == 9:
            rect_y = 124
            for i in range(7):
                rect_x = -5
                for j in range(8):
                    page.place(t.line(-5, rect_y, 5, rect_y))
                    page.place(t.rectangle(rect_x, rect_y+randint(-2,2), 0.6, 0.6))
                    rect_x += randint(0,2)
                rect_y += 5.25
            rect_y = 139.5
            for k in range(4):
                rect_x = 127
                for j in range(18):
                    page.place(t.line(127, rect_y, 135, rect_y))
                    page.place(t.rectangle(rect_x, rect_y+randint(-2,2), 0.6, 0.6))
                    rect_x += randint(0,2)
                rect_y += 5.3


        if folio == 11:
            y = 112
            for i in range(5):
                for i in range(40):
                    x = randint(-5, 3)
                    y1 = y+randint(0,3)
                    page.place(t.line(x, y1, x+randint(-3,3), y1))
                y += 5.3
            y = 127.5
            for i in range(4):
                for i in range(40):
                    x = randint(130, 135)
                    y1 = y+randint(0,3)
                    page.place(t.line(x, y1, x+randint(-3,3), y1))
                y += 5.3


        if folio == 16:
            y = 109
            for j in range(6):
                x = -4
                for i in range(5):
                    page.place(t.ellipse(x, y, 3, 3))
                    x += 1
                y += 5.3

            y = 135
            for i in range(5):
                x = -5
                for i in range(3):
                    x1 = x+randint(-2,2)
                    y1 = y+randint(-2,2)
                    x2 = x1+randint(-2,2)
                    y2 = y1+randint(-2,2)
                    page.place(t.line(x, y, x1, y1))
                    page.place(t.line(x1, y1, x2, y2))
                    page.place(t.line(x2, y2, x, y))
                    x += 3
                y += 5.3


            x = 126
            for i in range(12):
                y = 102
                x1 = x+randint(-2,2)
                y1 = y+randint(-2,2)
                x2 = x1+randint(-2,2)
                y2 = y1+randint(-2,2)
                page.place(t.line(x, y, x1, y1))
                page.place(t.line(x1, y1, x2, y2))
                page.place(t.line(x2, y2, x, y))
                x += 3

        if folio == 19:
            y = 103
            for j in range(2):
                x = -3
                for i in range(6):
                    page.place(t.ellipse(x, y, 3, 3))
                    x += 1
                y += 5.3


        if folio == 26:
            y = 103
            for k in range(5):
                for i in range(50):
                    x = -6
                    for i in range(4):
                        page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                        x += 2
                y += 5.3

        if folio == 27:
            y = 101.5

            for j in range(6):
                x = -6
                for i in range(3):
                    page.place(t.line(x, y, x+5, y+3))
                    page.place(t.line(x+5, y, x, y+3))
                    x += 3
                y += 5.3

        if folio == 28:
            y = 102.5
            for j in range(7):
                x = -5
                for i in range(8):
                    page.place(t.line(x, y, x+1, y+1))
                    page.place(t.line(x+1, y+1, x+1, y))
                    x += 1
                y += 5.3

        if folio == 29:
            y = 138
            for i in range(4):
                for i in range(40):
                    x = randint(-5, 2)
                    y1 = y+randint(0,3)
                    page.place(t.line(x, y1, x+randint(-3,3), y1))
                y += 5.3

            y = 101
            for i in range(4):
                for i in range(40):
                    x = randint(130, 135)
                    y1 = y+randint(0,3)
                    page.place(t.line(x, y1, x+randint(-3,3), y1))
                y += 5.3

        if folio == 34:
            rect_y = 103

            for i in range(4):
                rect_x = -3
                for j in range(10):
                    page.place(t.line(-5, rect_y, 2, rect_y))
                    page.place(t.rectangle(randint(rect_x, 2), rect_y+randint(-2,2), 0.6, 0.6))
                rect_y += 5.3

            rect_y = 108
            for i in range(7):
                rect_x = 125
                for j in range(10):
                    page.place(t.line(125, rect_y, 135, rect_y))
                    page.place(t.rectangle(randint(rect_x, 135), rect_y+randint(-2,2), 0.6, 0.6))
                rect_y += 5.3

        if folio == 36:
            y = 138.5
            for j in range(3):
                x = 125
                for i in range(5):
                    page.place(t.line(x, y, x+5, y+3))
                    page.place(t.line(x+5, y, x, y+3))
                    x += 2
                y += 5.3

        if folio == 40:
            y = 118.8
            for j in range(3):
                for i in range(50):
                    x = -4
                    for i in range(5):
                        page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                        x += 1
                y += 5.3

        if folio == 46:
            y = 101.8
            for i in range(4):
                for i in range(40):
                    x = randint(-2, 0)
                    y1 = y+randint(0,3)
                    page.place(t.line(x, y1, x+randint(-3,3), y1))
                y += 5.3
            y = 120
            for j in range(6):
                for i in range(20):
                    x = randint(-3, 2)
                    y1 = y + randint(1, 5)
                    page.place(t.circle(x, y1, 0.5))
                y += 5.3

        if folio == 52:
            y += 99.5
            for j in range(5):
                x = -5
                for i in range(7):
                    x1 = x+randint(-2,2)
                    y1 = y+randint(-2,2)
                    x2 = x1+randint(-2,2)
                    y2 = y1+randint(-2,2)
                    page.place(t.line(x, y, x1, y1))
                    page.place(t.line(x1, y1, x2, y2))
                    page.place(t.line(x2, y2, x, y))
                    x += 1
                y += 5.3

        if folio == 50:
            y = 111.8
            for i in range(6):
                for i in range(40):
                    x = randint(128, 135)
                    y1 = y+randint(0,3)
                    page.place(t.line(x, y1, x+randint(-3,3), y1))
                y += 5.3

        if folio == 55:
            y = 107.5
            for j in range(4):
                x = -3
                for i in range(8):
                    page.place(t.line(x, y, x+1, y+1))
                    page.place(t.line(x+1, y+1, x+1, y))
                    x += 1
                y += 5.3

        if folio == 61:
            y=116
            for j in range(8):
                for i in range(20):
                    x = randint(-5,4)
                    y1 = y + randint(1, 5)
                    page.place(t.circle(x, y1, 0.5))
                y += 5.3
            y=99.5
            for j in range(8):
                for i in range(20):
                    x = randint(128,135)
                    y1 = y + randint(1, 5)
                    page.place(t.circle(x, y1, 0.5))
                y += 5.3

        if folio == 64:
            y=99.5
            for j in range(5):
                for i in range(20):
                    x = randint(-5,2)
                    y1 = y + randint(1, 5)
                    page.place(t.circle(x, y1, 0.5))
                y += 5.3
            y=99.5
            for j in range(6):
                for i in range(20):
                    x = randint(126,135)
                    y1 = y + randint(1, 5)
                    page.place(t.circle(x, y1, 0.5))
                y += 5.3

        if folio == 65:
            y = 103
            for j in range(11):
                x = -3
                for i in range(8):
                    page.place(t.line(x, y, x+1, y+1))
                    page.place(t.line(x+1, y+1, x+1, y))
                    x += 1
                y += 5.3
            y = 128.3
            for j in range(6):
                for i in range(52):
                    x = 130
                    for i in range(8):
                        page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                        x += 1
                y+= 5.3

        if folio == 67:
            y = 118.5
            for j in range(8):
                for i in range(52):
                    x = 130
                    for i in range(8):
                        page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                        x += 1
                y+= 5.3

        if folio == 72:
            y = 100.5
            for i in range(11):
                for i in range(40):
                    x = randint(-3, 0)
                    y1 = y+randint(0,3)
                    page.place(t.line(x, y1, x+randint(-3,3), y1))
                y += 5.3
            y = 100.5
            for i in range(7):
                for i in range(40):
                    x = randint(128, 135)
                    y1 = y+randint(0,3)
                    page.place(t.line(x, y1, x+randint(-3,3), y1))
                y += 5.3

        if folio == 73:
            y = 102
            for i in range(11):
                x = -7
                for i in range(11):
                    x1 = x+randint(-2,2)
                    y1 = y+randint(-2,2)
                    x2 = x1+randint(-2,2)
                    y2 = y1+randint(-2,2)
                    page.place(t.line(x, y, x1, y1))
                    page.place(t.line(x1, y1, x2, y2))
                    page.place(t.line(x2, y2, x, y))
                    x += 1
                y += 5.3
            y = 102
            for i in range(7):
                x = 129
                for i in range(11):
                    x1 = x+randint(-2,2)
                    y1 = y+randint(-2,2)
                    x2 = x1+randint(-2,2)
                    y2 = y1+randint(-2,2)
                    page.place(t.line(x, y, x1, y1))
                    page.place(t.line(x1, y1, x2, y2))
                    page.place(t.line(x2, y2, x, y))
                    x += 1
                y += 5.3

        if folio == 75:
            y=115.5
            for j in range(8):
                for i in range(20):
                    x = randint(-5,4)
                    y1 = y + randint(1, 5)
                    page.place(t.circle(x, y1, 0.5))
                y += 5.3
            y = 112.5
            for i in range(7):
                x = 130
                for i in range(5):
                    page.place(t.ellipse(x, y, 3, 3))
                    x += 1
                y += 5.3


        if folio % 2 == 0: #page paire en début d'article
            block = page.place(text(pieces))
            titre.frame(8, 5, 130, 100)
            tl = titre.frame(8, 5, 130, 100).lines()
            make_index(tl, index_entries, folio)
            if len(tl) == 2:
                auteur.frame(15, 39, 110, 15)
            elif len(tl) == 3:
                auteur.frame(15, 54, 110, 15)
            elif len(tl) == 4:
                auteur.frame(15, 69, 110, 15)
            else:
                auteur.frame(15, 85, 110, 15)
            #if folio % 2 == 0:


            block.frame(x_pg_col1, y_bloc1, w_blocs, h_bloc1)
            linesb1 = block.frame(x_pg_col1, y_bloc1, w_blocs, h_bloc1).lines()
            make_index(linesb1, index_entries, folio)
            make_name_index(linesb1, index_names, folio)
            categob1 = search_categoriesG(linesb1)

                #str_cat1 = '\n'.join(catego)
            for x in categob1.keys():
                y = categob1.get(x) #value
                if x == 'Creativity/Arts':
                    arts.append(folio)
                    cat_page.append((x, y, page, folio, "colP1"))
                elif x == 'Feminism':
                    feminism.append(folio)
                    cat_page.append((x, y, page, folio, "colP1"))
                elif x == 'Security/Encryption':
                    security.append(folio)
                    cat_page.append((x, y, page, folio, "colP1"))
                elif x == 'Programming':
                    programming.append(folio)
                    cat_page.append((x, y, page, folio, "colP1"))
                elif x == 'Games':
                    games.append(folio)
                    cat_page.append((x, y, page, folio, "colP1"))
                elif x == 'Free & Open source':
                    open_source.append(folio)
                    cat_page.append((x, y, page, folio, "colP1"))
                elif x == 'DIY':
                    diy.append(folio)
                    cat_page.append((x, y, page, folio, "colP1"))
                elif x == 'Hardware':
                    hardware.append(folio)
                    cat_page.append((x, y, page, folio, "colP1"))

                #place_categoriesP1(folio, block_des_notes, y)


            foliotage = page.place(folios.text("%s" % folio))
            foliotage.frame(11, 169, 20, 20)

            if block.frame(x_pg_col1, y_bloc1, w_blocs, h_bloc1).overflow():
                block = page.chain(block)
                block.frame(x_pg_col2, y_bloc1+4.9, w_blocs, h_bloc1-5)
                linesb2 = block.frame(x_pg_col2, y_bloc1, w_blocs, h_bloc1).lines()
                make_index(linesb2, index_entries, folio)
                make_name_index(linesb2, index_names, folio)
                categob2 = search_categoriesG(linesb2)

                    #str_cat1 = '\n'.join(catego)
                for x in categob2.keys():
                    y = categob2.get(x) #value
                    if x == 'Creativity/Arts':
                        arts.append(folio)
                        cat_page.append((x, y, page, folio, "colP2"))
                    elif x == 'Feminism':
                        feminism.append(folio)
                        cat_page.append((x, y, page, folio, "colP2"))
                    elif x == 'Security/Encryption':
                        security.append(folio)
                        cat_page.append((x, y, page, folio, "colP2"))
                    elif x == 'Programming':
                        programming.append(folio)
                        cat_page.append((x, y, page, folio, "colP2"))
                    elif x == 'Games':
                        games.append(folio)
                        cat_page.append((x, y, page, folio, "colP2"))
                    elif x == 'Free & Open source':
                        open_source.append(folio)
                        cat_page.append((x, y, page, folio, "colP2"))
                    elif x == 'DIY':
                        diy.append(folio)
                        cat_page.append((x, y, page, folio, "colP2"))
                    elif x == 'Hardware':
                        hardware.append(folio)
                        cat_page.append((x, y, page, folio, "colP2"))


            if block.frame(x_pg_col2, y_bloc1+4.9, w_blocs, h_bloc1-5).overflow():
                page = doc.addpage()
                folio += 1


                if folio == 17:
                    y = 5.8

                    for j in range(14):
                        x = 130
                        for i in range(6):
                            page.place(t.ellipse(x, y, 3, 3))
                            x += 1
                        y += 5.3

                    y = 38
                    for i in range(4):
                        x = -6
                        for i in range(4):
                            x1 = x+randint(-2,2)
                            y1 = y+randint(-2,2)
                            x2 = x1+randint(-2,2)
                            y2 = y1+randint(-2,2)
                            page.place(t.line(x, y, x1, y1))
                            page.place(t.line(x1, y1, x2, y2))
                            page.place(t.line(x2, y2, x, y))
                            x += 3
                        y += 5.3

                    y = 122.5
                    for i in range(7):
                        x = -6
                        for i in range(4):
                            x1 = x+randint(-2,2)
                            y1 = y+randint(-2,2)
                            x2 = x1+randint(-2,2)
                            y2 = y1+randint(-2,2)
                            page.place(t.line(x, y, x1, y1))
                            page.place(t.line(x1, y1, x2, y2))
                            page.place(t.line(x2, y2, x, y))
                            x += 3
                        y += 5.3

                    y = 83
                    for j in range(3):
                        for i in range(20):
                            x = randint(-3, 5)
                            y1 = y + randint(1, 5)
                            page.place(t.circle(x, y1, 0.5))
                        y += 5.3

                if folio == 37:
                    y = 91
                    for i in range(8):
                        x = -4
                        for i in range(7):
                            page.place(t.ellipse(x, y, 3, 3))
                            x += 1
                        y += 5.3

                    y = 122.3
                    for i in range(3):
                        x = 130
                        for i in range(7):
                            page.place(t.ellipse(x, y, 3, 3))
                            x += 1
                        y += 5.3

                if folio == 41:
                    y = 134
                    for j in range(5):
                        for i in range(50):
                            x = -3
                            for i in range(6):
                                page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                                x += 1
                        y += 5.3

                    y = 6
                    for i in range(8):
                        x = 130
                        for i in range(16):
                            x1 = x+randint(-2,2)
                            y1 = y+randint(-2,2)
                            x2 = x1+randint(-2,2)
                            y2 = y1+randint(-2,2)
                            page.place(t.line(x, y, x1, y1))
                            page.place(t.line(x1, y1, x2, y2))
                            page.place(t.line(x2, y2, x, y))
                            x += 3
                        y += 5.3

                if folio == 47:
                    y = 15
                    for j in range(10):
                        for i in range(20):
                            x = randint(-3, 5)
                            y1 = y + randint(1, 5)
                            page.place(t.circle(x, y1, 0.5))
                        y += 5.3
                    y += 5.3
                    for j in range(16):
                        for i in range(20):
                            x = randint(-3, 5)
                            y1 = y + randint(1, 5)
                            page.place(t.circle(x, y1, 0.5))
                        y += 5.3
                    y = 4.5
                    for j in range(8):
                        for i in range(20):
                            x = randint(128, 135)
                            y1 = y + randint(1, 5)
                            page.place(t.circle(x, y1, 0.5))
                        y += 5.3

                if folio == 51:
                    y = 21.5
                    for i in range(4):
                        for i in range(40):
                            x = randint(130, 135)
                            y1 = y+randint(0,3)
                            page.place(t.line(x, y1, x+randint(-3,3), y1))
                        y += 5.3
                    y=31
                    for j in range(3):
                        for i in range(20):
                            x = randint(-5,4)
                            y1 = y + randint(1, 5)
                            page.place(t.circle(x, y1, 0.5))
                        y += 5.3
                    legende1 = "Schema of Docker interfaces"
                    legende1_txt = page.place(legende.text(legende1))
                    legende1_txt.frame(84, 102, 40, 50)

                if folio == 53:
                    y = 48.5
                    for j in range(4):
                        for i in range(50):
                            x = -4
                            for i in range(8):
                                page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                                x += 1
                        y+= 5.3
                    y = 81.3
                    for i in range(3):
                        x = -6
                        for i in range(11):
                            x1 = x+randint(-2,2)
                            y1 = y+randint(-2,2)
                            x2 = x1+randint(-2,2)
                            y2 = y1+randint(-2,2)
                            page.place(t.line(x, y, x1, y1))
                            page.place(t.line(x1, y1, x2, y2))
                            page.place(t.line(x2, y2, x, y))
                            x += 1
                        y += 5.3
                    y = 28
                    for i in range(2):
                        x = 130
                        for i in range(13):
                            page.place(t.ellipse(x, y, 3, 3))
                            x += 1
                        y += 5.3
                    legende1 = "Screenshots of Aspirina issue n°17, spring 2018"
                    legende1_txt = page.place(legende.text(legende1))
                    legende1_txt.frame(84, 144, 40, 50)




                pages.append(folio)
                block = page.chain(block)
                block.frame(x_pd_col1, y_bloc_standard, w_blocs, h_bloc_standard)

                col = 1

                while block.frame(x_pd_col1, y_bloc_standard, w_blocs, h_bloc_standard).overflow():

                    if col == 0:

                        if folio % 2 == 0:
                            block.frame(x_pg_col2, y_bloc_standard, w_blocs, h_bloc_standard)
                            lines = block.frame(x_pg_col2, y_bloc_standard, w_blocs, h_bloc_standard).lines()
                            make_index(lines, index_entries, folio)
                            make_name_index(lines, index_names, folio)
                            catego = search_categoriesG(lines)

                                    #str_cat1 = '\n'.join(catego)
                            for x in catego.keys():
                                y = catego.get(x) #value
                                if x == 'Creativity/Arts':
                                    arts.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'Feminism':
                                    feminism.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'Security/Encryption':
                                    security.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'Programming':
                                    programming.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'Games':
                                    games.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'Free & Open source':
                                    open_source.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'DIY':
                                    diy.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'Hardware':
                                    hardware.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))

                        else:

                            block.frame(x_pd_col2, y_bloc_standard, w_blocs, h_bloc_standard)
                            lines = block.frame(x_pd_col2, y_bloc_standard, w_blocs, h_bloc_standard).lines()
                            make_index(lines, index_entries, folio)
                            make_name_index(lines, index_names, folio)
                            catego = search_categoriesD(lines)

                            for x in catego.keys():
                                y = catego.get(x) #value
                                if x == 'Creativity/Arts':
                                    arts.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'Feminism':
                                    feminism.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'Security/Encryption':
                                    security.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'Programming':
                                    programming.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'Games':
                                    games.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'Free & Open source':
                                    open_source.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'DIY':
                                    diy.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'Hardware':
                                    hardware.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))

                                #place_categories(folio, block_des_notes, y)
                                        #print(lines[10]

                        page = doc.addpage()
                        folio += 1


                        if folio == 38:
                            rect_y = 112.5
                            for i in range(9):
                                rect_x = -5
                                for j in range(10):
                                    page.place(t.line(-5, rect_y, 2, rect_y))
                                    page.place(t.rectangle(randint(rect_x, 2), rect_y+randint(-2,2), 0.6, 0.6))
                                rect_y += 5.3
                            rect_y = 7
                            for i in range(13):
                                rect_x = 125
                                for j in range(10):
                                    page.place(t.line(125, rect_y, 135, rect_y))
                                    page.place(t.rectangle(randint(rect_x, 135), rect_y+randint(-2,2), 0.6, 0.6))
                                rect_y += 5.3

                            y = 111.5
                            for i in range(9):
                                x = 128
                                for i in range(13):
                                    page.place(t.ellipse(x, y, 3, 3))
                                    x += 1
                                y += 5.3

                        if folio == 42:
                            y = 55
                            for j in range(8):
                                x = 127
                                for i in range(16):
                                    x1 = x+randint(-2,2)
                                    y1 = y+randint(-2,2)
                                    x2 = x1+randint(-2,2)
                                    y2 = y1+randint(-2,2)
                                    page.place(t.line(x, y, x1, y1))
                                    page.place(t.line(x1, y1, x2, y2))
                                    page.place(t.line(x2, y2, x, y))
                                    x += 3
                                y += 5.3

                        if folio == 43:
                            y = 80.8
                            for j in range(10):
                                x = -6
                                for i in range(4):
                                    x1 = x+randint(-2,2)
                                    y1 = y+randint(-2,2)
                                    x2 = x1+randint(-2,2)
                                    y2 = y1+randint(-2,2)
                                    page.place(t.line(x, y, x1, y1))
                                    page.place(t.line(x1, y1, x2, y2))
                                    page.place(t.line(x2, y2, x, y))
                                    x += 3
                                y += 5.3

                        if folio == 44:
                            y = 6.5
                            for j in range(9):
                                x = -6
                                for i in range(3):
                                    x1 = x+randint(-2,2)
                                    y1 = y+randint(-2,2)
                                    x2 = x1+randint(-2,2)
                                    y2 = y1+randint(-2,2)
                                    page.place(t.line(x, y, x1, y1))
                                    page.place(t.line(x1, y1, x2, y2))
                                    page.place(t.line(x2, y2, x, y))
                                    x += 3
                                y += 5.3

                            y = 76
                            for j in range(5):
                                x = 127
                                for i in range(4):
                                    x1 = x+randint(-2,2)
                                    y1 = y+randint(-2,2)
                                    x2 = x1+randint(-2,2)
                                    y2 = y1+randint(-2,2)
                                    page.place(t.line(x, y, x1, y1))
                                    page.place(t.line(x1, y1, x2, y2))
                                    page.place(t.line(x2, y2, x, y))
                                    x += 3
                                y += 5.3

                        if folio == 48:
                            y = 47.3
                            for i in range(5):
                                for i in range(40):
                                    x = randint(-3, 0)
                                    y1 = y+randint(0,3)
                                    page.place(t.line(x, y1, x+randint(-3,3), y1))
                                y += 5.3
                            y = 141
                            for j in range(3):
                                for i in range(20):
                                    x = randint(-3, 2)
                                    y1 = y + randint(1, 5)
                                    page.place(t.circle(x, y1, 0.5))
                                y += 5.3
                            y = 4
                            for j in range(6):
                                for i in range(20):
                                    x = randint(125, 135)
                                    y1 = y + randint(1, 5)
                                    page.place(t.circle(x, y1, 0.5))
                                y += 5.3
                            legende1 = "On right page: screenshot of the /ETC website"
                            legende1_txt = page.place(legende.text(legende1))
                            legende1_txt.frame(84, 45, 30, 50)


                        col = 1
                    elif col == 1:

                        if folio % 2 == 0:
                            tcg = a["titreS"]
                            tcg_txt = page.place(titre_courant.text(tcg))
                            if tcg == "Build and reach the Internet with an antenna from the roof ":
                                tcg_txt.frame(63,161,60,30)
                            elif tcg == "A Brief History of feminism and technology " :
                                tcg_txt.frame(63,161,40,30)
                            elif tcg == "Create a website that cannot be hacked but looks awesome! " :
                                tcg_txt.frame(63,161,60,30)
                            else:
                                tcg_txt.frame(63,170,68,30)
                            block.frame(x_pg_col1, y_bloc_standard, w_blocs, h_bloc_standard)
                            foliotage = page.place(folios.text("%s" % folio))
                            foliotage.frame(11, 169, 20, 20)
                            lines = block.frame(x_pg_col1, y_bloc_standard, w_blocs, h_bloc_standard).lines()
                            make_index(lines, index_entries, folio)
                            make_name_index(lines, index_names, folio)
                            catego = search_categoriesG(lines)

                                    #str_cat1 = '\n'.join(catego)
                            for x in catego.keys():
                                y = catego.get(x) #value
                                if x == 'Creativity/Arts':
                                    arts.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'Feminism':
                                    feminism.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'Security/Encryption':
                                    security.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'Programming':
                                    programming.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'Games':
                                    games.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'Free & Open source':
                                    open_source.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'DIY':
                                    diy.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'Hardware':
                                    hardware.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                #place_categories(folio, block_des_notes, y)

                        else:

                            block.frame(x_pd_col1, y_bloc_standard, w_blocs, h_bloc_standard)
                            tcd = a["auteur"]
                            tcd_txt = page.place(titre_courant.text(tcd))
                            tcd_txt.frame(23,170,75,30)
                            foliotage = page.place(folios.text("%s" % folio))
                            foliotage.frame(88, 169, 20, 20)
                            lines = block.frame(x_pd_col1, y_bloc_standard, w_blocs, h_bloc_standard).lines()
                            make_index(lines, index_entries, folio)
                            make_name_index(lines, index_names, folio)
                            catego = search_categoriesD(lines)

                                #str_cat1 = '\n'.join(catego)
                            for x in catego.keys():
                                y = catego.get(x) #value
                                if x == 'Creativity/Arts':
                                    arts.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'Feminism':
                                    feminism.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'Security/Encryption':
                                    security.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'Programming':
                                    programming.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'Games':
                                    games.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'Free & Open source':
                                    open_source.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'DIY':
                                    diy.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'Hardware':
                                    hardware.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                #place_categories(folio, block_des_notes, y)
                                        #print(lines[10]


                        col = 0
                    block = page.chain(block)

                        #folio = folio + 1
                else: #si pas overflow bloc standard =  fin article


                    if folio == 18:
                        y = 22.8
                        for j in range(7):
                            x = 128
                            for i in range(7):
                                page.place(t.ellipse(x, y, 3, 3))
                                x += 1
                            y += 5.3
                        legende1 = "Screenshots of projects made with UpStage"
                        legende1_txt = page.place(legende.text(legende1))
                        legende1_txt.frame(12, 129, 40, 50)


                    if folio == 35:
                        rect_y = 12
                        for i in range(5):
                            rect_x = -5
                            for j in range(10):
                                page.place(t.line(-5, rect_y, 5, rect_y))
                                page.place(t.rectangle(randint(rect_x, 5), rect_y+randint(-2,2), 0.6, 0.6))
                            rect_y += 5.3
                        rect_y = 7
                        for i in range(5):
                            rect_x = 127
                            for j in range(10):
                                page.place(t.line(127, rect_y, 135, rect_y))
                                page.place(t.rectangle(randint(rect_x, 135), rect_y+randint(-2,2), 0.6, 0.6))
                            rect_y += 5.3

                    if folio == 39:
                        y = 5.5
                        for i in range(10):
                            x = 130
                            for i in range(13):
                                page.place(t.ellipse(x, y, 3, 3))
                                x += 1
                            y += 5.3
                        rect_y = 91.5
                        for i in range(7):
                            rect_x = 127
                            for j in range(10):
                                page.place(t.line(127, rect_y, 135, rect_y))
                                page.place(t.rectangle(randint(rect_x, 135), rect_y+randint(-2,2), 0.6, 0.6))
                            rect_y += 5.3
                        legende1 = "Map of Ninux nodes around Bologna"
                        legende1_txt = page.place(legende.text(legende1))
                        legende1_txt.frame(15, 48, 35, 50)

                    if folio == 45:
                        y = 49
                        for j in range(4):
                            x = -5
                            for i in range(4):
                                x1 = x+randint(-2,2)
                                y1 = y+randint(-2,2)
                                x2 = x1+randint(-2,2)
                                y2 = y1+randint(-2,2)
                                page.place(t.line(x, y, x1, y1))
                                page.place(t.line(x1, y1, x2, y2))
                                page.place(t.line(x2, y2, x, y))
                                x += 3
                            y += 5.3


                    if folio % 2 == 0:

                        block.frame(x_pg_col1, y_bloc_standard, w_blocs, h_bloc_standard)

                        foliotage = page.place(folios.text("%s" % folio))
                        foliotage.frame(11, 169, 20, 20)
                        lines = block.frame(x_pg_col1, y_bloc_standard, w_blocs, h_bloc_standard).lines()
                        make_index(lines, index_entries, folio)
                        make_name_index(lines, index_names, folio)
                        catego = search_categoriesG(lines)

                                #str_cat1 = '\n'.join(catego)
                        for x in catego.keys():
                            y = catego.get(x) #value
                            if x == 'Creativity/Arts':
                                arts.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'Feminism':
                                feminism.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'Security/Encryption':
                                security.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'Programming':
                                programming.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'Games':
                                games.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'Free & Open source':
                                open_source.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'DIY':
                                diy.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'Hardware':
                                hardware.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))

                            #place_categories(folio, block_des_notes, y)

                        if 'imgIn' in a:
                            imgin = image.open(a.get('imgIn'))
                            block_imgin = page.place(imgin)
                            block_imgin.frame(-3,5,62,160)
                            block_imgin.fitwidth(62)

                        if 'imgFin' in a:
                            page = doc.addpage()
                            folio += 1
                            imgFin = image.open(a.get('imgFin'))
                            block_imgfin = page.place(imgFin)
                            if folio == 77:
                                block_imgfin.frame(-5,-3,145,160)
                                block_imgfin.fitwidth(145)

                            else:
                                block_imgfin.frame(-1,-8,134,160)
                                block_imgfin.fitwidth(134)

                        if block.frame(x_pg_col2, y_bloc_standard, w_blocs, h_bloc_standard).overflow():
                            block = page.chain(block)
                            block.frame(x_pg_col1, y_bloc_standard, w_blocs, h_bloc_standard)

                    else:
                        block.frame(x_pd_col2, y_bloc_standard, w_blocs, h_bloc_standard)
                        tcd = a["auteur"]
                        tcd_txt = page.place(titre_courant.text(tcd))
                        tcd_txt.frame(23,170,75,30)
                        foliotage = page.place(folios.text("%s" % folio))
                        foliotage.frame(88, 169, 20, 20)
                        lines = block.frame(x_pd_col2, y_bloc_standard, w_blocs, h_bloc_standard).lines()
                        make_index(lines, index_entries, folio)
                        make_name_index(lines, index_names, folio)
                        catego = search_categoriesD(lines)
                            #str_cat1 = '\n'.join(catego)
                        for x in catego.keys():
                            y = catego.get(x) #value
                            if x == 'Creativity/Arts':
                                arts.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'Feminism':
                                feminism.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'Security/Encryption':
                                security.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'Programming':
                                programming.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'Games':
                                games.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'Free & Open source':
                                open_source.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'DIY':
                                diy.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'Hardware':
                                hardware.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            #place_categories(folio, block_des_notes, y)
                        if 'imgIn' in a:
                            imgin = image.open(a.get('imgIn'))
                            block_imgin = page.place(imgin)
                            if folio == 39:
                                block_imgin.frame(-1,5,64,160)
                                block_imgin.fitwidth(64)
                            elif folio == 51:
                                block_imgin.frame(73,48,60,160)
                                block_imgin.fitwidth(60)
                            elif folio == 53:
                                block_imgin.frame(73,75,58,160)
                                block_imgin.fitwidth(58)
                            else:
                                block_imgin.frame(-3,5,65,160)
                                block_imgin.fitwidth(65)




            #else:
                #block.frame(22, 90, 85, 60)

        else: #page impaire au début d'article


            block = page.place(text(pieces))
            titre.frame(8, 5, 130, 100)
            tl = titre.frame(8, 5, 130, 100).lines()
            make_index(tl, index_entries, folio)
            foliotage = page.place(folios.text("%s" % folio))
            foliotage.frame(88, 169, 20, 20)
            #folio += 1
            if len(tl) == 2:
                auteur.frame(18, 39, 110, 15)
            elif len(tl) == 3:
                auteur.frame(18, 54, 110, 15)
            elif len(tl) == 4:
                auteur.frame(18, 69, 110, 15)
            else:
                auteur.frame(18, 85, 110, 15)

            block.frame(x_pd_col1, y_bloc1, w_blocs, h_bloc1)
            linesb1 = block.frame(x_pd_col1, y_bloc1, w_blocs, h_bloc1).lines()
            make_index(linesb1, index_entries, folio)
            make_name_index(linesb1, index_names, folio)
            categob1 = search_categoriesD(linesb1)

                #str_cat1 = '\n'.join(catego)
            for x in categob1.keys():
                y = categob1.get(x) #value
                if x == 'Creativity/Arts':
                    arts.append(folio)
                    cat_page.append((x, y, page, folio, "colP1"))
                elif x == 'Feminism':
                    feminism.append(folio)
                    cat_page.append((x, y, page, folio, "colP1"))
                elif x == 'Security/Encryption':
                    security.append(folio)
                    cat_page.append((x, y, page, folio, "colP1"))
                elif x == 'Programming':
                    programming.append(folio)
                    cat_page.append((x, y, page, folio, "colP1"))
                elif x == 'Games':
                    games.append(folio)
                    cat_page.append((x, y, page, folio, "colP1"))
                elif x == 'Free & Open source':
                    open_source.append(folio)
                    cat_page.append((x, y, page, folio, "colP1"))
                elif x == 'DIY':
                    diy.append(folio)
                    cat_page.append((x, y, page, folio, "colP1"))
                elif x == 'Hardware':
                    hardware.append(folio)
                    cat_page.append((x, y, page, folio, "colP1"))
                #place_categoriesP1(folio, block_des_notes, y)



            if block.frame(x_pd_col1, y_bloc1, w_blocs, h_bloc1).overflow():
                block = page.chain(block)
                block.frame(x_pd_col2, y_bloc1+4.9, w_blocs, h_bloc1-5)
                linesb2 = block.frame(x_pd_col2, y_bloc1, w_blocs, h_bloc1).lines()
                make_index(linesb2, index_entries, folio)
                make_name_index(linesb2, index_names, folio)
                categob2 = search_categoriesD(linesb2)

                    #str_cat1 = '\n'.join(catego)
                for x in categob2.keys():
                    y = categob2.get(x) #value
                    if x == 'Creativity/Arts':
                        arts.append(folio)
                        cat_page.append((x, y, page, folio, "colP2"))
                    elif x == 'Feminism':
                        feminism.append(folio)
                        cat_page.append((x, y, page, folio, "colP2"))
                    elif x == 'Security/Encryption':
                        security.append(folio)
                        cat_page.append((x, y, page, folio, "colP2"))
                    elif x == 'Programming':
                        programming.append(folio)
                        cat_page.append((x, y, page, folio, "colP2"))
                    elif x == 'Games':
                        games.append(folio)
                        cat_page.append((x, y, page, folio, "colP2"))
                    elif x == 'Free & Open source':
                        open_source.append(folio)
                        cat_page.append((x, y, page, folio, "colP2"))
                    elif x == 'DIY':
                        diy.append(folio)
                        cat_page.append((x, y, page, folio, "colP2"))
                    elif x == 'Hardware':
                        hardware.append(folio)
                        cat_page.append((x, y, page, folio, "colP2"))

            else:
                block.frame(x_pd_col1, y_bloc1, w_blocs, h_bloc1)
                linesb1 = block.frame(x_pd_col1, y_bloc1, w_blocs, h_bloc1).lines()
                make_index(linesb1, index_entries, folio)
                make_name_index(linesb1, index_names, folio)
                categob1 = search_categoriesD(linesb1)

                    #str_cat1 = '\n'.join(catego)
                for x in categob1.keys():
                    y = categob1.get(x) #value
                    if x == 'Creativity/Arts':
                        arts.append(folio)
                        cat_page.append((x, y, page, folio, "colP1"))
                    elif x == 'Feminism':
                        feminism.append(folio)
                        cat_page.append((x, y, page, folio, "colP1"))
                    elif x == 'Security/Encryption':
                        security.append(folio)
                        cat_page.append((x, y, page, folio, "colP1"))
                    elif x == 'Programming':
                        programming.append(folio)
                        cat_page.append((x, y, page, folio, "colP1"))
                    elif x == 'Games':
                        games.append(folio)
                        cat_page.append((x, y, page, folio, "colP1"))
                    elif x == 'Free & Open source':
                        open_source.append(folio)
                        cat_page.append((x, y, page, folio, "colP1"))
                    elif x == 'DIY':
                        diy.append(folio)
                        cat_page.append((x, y, page, folio, "colP1"))
                    elif x == 'Hardware':
                        hardware.append(folio)
                        cat_page.append((x, y, page, folio, "colP1"))


            if block.frame(x_pd_col2, y_bloc1+4.9, w_blocs, h_bloc1-5).overflow():
                page = doc.addpage()

                folio += 1
                if folio == 10:
                    legende1 = "Picture of the workshop"
                    legende1_txt = page.place(legende.text(legende1))
                    legende1_txt.frame(12, 54, 50, 50)
                    rect_y = 6
                    for k in range(4):
                        rect_x = 125
                        for j in range(18):
                            page.place(t.line(125, rect_y, 135, rect_y))
                            page.place(t.rectangle(rect_x, rect_y+randint(-2,2), 0.6, 0.6))
                            rect_x += randint(0,2)
                        rect_y += 5.3

                if folio == 12:
                    for i in range(20):
                        x = randint(-2, 0)
                        y = 10+randint(0,3)
                        page.place(t.line(x, y, x+randint(-3,3), y))
                    for i in range(8):
                        x = randint(-2, 0)
                        y = 16+randint(0,3)
                        page.place(t.line(x, y, x+randint(-3,3), y))
                    y = 105.5
                    for i in range(5):
                        for i in range(20):
                            x = randint(127, 135)
                            y1 = y+randint(0,3)
                            page.place(t.line(x, y1, x+randint(-3,3), y1))
                        y += 5.3
                    y1 = 132
                    for j in range(3):
                        for i in range(20):
                            x = randint(125, 135)
                            y = y1+randint(0,3)
                            page.place(t.circle(x, y, 0.5))
                        y1+= 5.3


                if folio == 20:
                    y = 18.2
                    for i in range(4):
                        x = -4
                        for i in range(5):
                            page.place(t.ellipse(x, y, 3, 3))
                            x += 1
                        y += 5.3

                    y = 33.5
                    for j in range(18):
                        x = -6
                        for k in range(4):
                            for i in range(52):
                                page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                            x += 2
                        y += 5.3

                    y = 120
                    for j in range(7):
                        for i in range(20):
                            x = randint(-5, 2)
                            y1 = y + randint(1, 5)
                            page.place(t.circle(x, y1, 0.5))
                        y += 5.3
                    y = 5
                    for i in range(3):
                        for j in range(15):
                            x = randint(125, 135)
                            y1 = y + randint(1, 5)
                            page.place(t.circle(x, y1, 0.5))
                        y += 5.3

                    y = 28
                    for j in range(8):
                        x = 128
                        for k in range(23):
                            for i in range(50):
                                page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                            x += 2
                        y += 5

                    for i in range(4):
                        x = 128
                        for i in range(14):
                            page.place(t.ellipse(x, y, 3, 3))
                            x += 1
                        y += 5.3
                    y = 149.5
                    for i in range(2):
                        x = 128
                        for i in range(14):
                            page.place(t.ellipse(x, y, 3, 3))
                            x += 1
                        y += 5.3

                if folio == 30:
                    y = 100
                    for i in range(5):
                        for i in range(20):
                            x = randint(-3, 0)
                            y1 = y+randint(0,3)
                            page.place(t.line(x, y1, x+randint(-3,3), y1))
                        y += 5.3

                    y = 31.5
                    for i in range(5):
                        for i in range(20):
                            x = randint(128, 135)
                            y1 = y+randint(0,3)
                            page.place(t.line(x, y1, x+randint(-3,3), y1))
                        y += 5.3

                if folio == 56:
                    y = 28
                    for j in range(5):
                        for i in range(52):
                            x = 128
                            for i in range(8):
                                page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                                x += 1
                        y+= 5.3

                if folio == 62:
                    y=114.5
                    for j in range(8):
                        for i in range(20):
                            x = randint(-5,2)
                            y1 = y + randint(1, 5)
                            page.place(t.circle(x, y1, 0.5))
                        y += 5.3
                    y=4
                    for j in range(14):
                        for i in range(20):
                            x = randint(126,135)
                            y1 = y + randint(1, 5)
                            page.place(t.circle(x, y1, 0.5))
                        y += 5.3
                    legende1 = "On right page: Chromium web inspector in use on the /ETC website"
                    legende1_txt = page.place(legende.text(legende1))
                    legende1_txt.frame(84, 150, 40, 50)

                if folio == 68:
                    y = 6
                    for j in range(10):
                        for i in range(52):
                            x = -7
                            for i in range(8):
                                page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                                x += 1
                        y+= 5.3
                    y = 80.5
                    for j in range(15):
                        for i in range(52):
                            x = -7
                            for i in range(8):
                                page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                                x += 1
                        y+= 5.3
                    y = 6
                    for j in range(28):
                        for i in range(52):
                            x = 128
                            for i in range(8):
                                page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                                x += 1
                        y+= 5.3

                if folio == 76:
                    y=68
                    for j in range(12):
                        for i in range(20):
                            x = randint(-5,2)
                            y1 = y + randint(1, 5)
                            page.place(t.circle(x, y1, 0.5))
                        y += 5.3
                    y = 6.5
                    for i in range(6):
                        x = -4
                        for i in range(5):
                            page.place(t.ellipse(x, y, 3, 3))
                            x += 1
                        y += 5.3
                    y=19.5
                    for j in range(11):
                        for i in range(20):
                            x = randint(126,135)
                            y1 = y + randint(1, 5)
                            page.place(t.circle(x, y1, 0.5))
                        y += 5.3


                pages.append(folio)
                block = page.chain(block)
                block.frame(x_pg_col1, y_bloc_standard, w_blocs, h_bloc_standard)

                col = 1

                while block.frame(x_pg_col1, y_bloc_standard, w_blocs, h_bloc_standard).overflow():

                    if folio == 13:
                        y = 4.5
                        for i in range(15):
                            for i in range(15):
                                x = randint(-5, 2)
                                y1 = y+randint(0,3)
                                page.place(t.line(x, y1, x+randint(-3,3), y1))
                            y += 5.3
                        y = 105.8
                        for i in range(4):
                            for i in range(15):
                                x = randint(130, 135)
                                y1 = y+randint(0,3)
                                page.place(t.line(x, y1, x+randint(-3,3), y1))
                            y += 5.3
                        y =  121.5
                        for j in range(6):
                            for i in range(10):
                                x = randint(-3, 4)
                                y1 = y + randint(0,4)
                                page.place(t.circle(x, y1, 0.5))
                            y += 5.3



                    if col == 0:

                        if folio % 2 == 0:

                            block.frame(x_pg_col2, y_bloc_standard, w_blocs, h_bloc_standard)
                            lines = block.frame(x_pg_col2, y_bloc_standard, w_blocs, h_bloc_standard).lines()
                            make_index(lines, index_entries, folio)
                            make_name_index(lines, index_names, folio)
                            catego = search_categoriesG(lines)

                            #str_cat1 = '\n'.join(catego)
                            for x in catego.keys():
                                y = catego.get(x) #value
                                if x == 'Creativity/Arts':
                                    arts.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'Feminism':
                                    feminism.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'Security/Encryption':
                                    security.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'Programming':
                                    programming.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'Games':
                                    games.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'Free & Open source':
                                    open_source.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'DIY':
                                    diy.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'Hardware':
                                    hardware.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))

                                #place_categories(folio, block_des_notes, y)
                        else:

                            block.frame(x_pd_col2, y_bloc_standard, w_blocs, h_bloc_standard)
                            lines = block.frame(x_pd_col2, y_bloc_standard, w_blocs, h_bloc_standard).lines()
                            make_index(lines, index_entries, folio)
                            make_name_index(lines, index_names, folio)
                            catego = search_categoriesD(lines)

                            #str_cat1 = '\n'.join(catego)
                            for x in catego.keys():
                                y = catego.get(x) #value
                                if x == 'Creativity/Arts':
                                    arts.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'Feminism':
                                    feminism.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'Security/Encryption':
                                    security.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'Programming':
                                    programming.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'Games':
                                    games.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'Free & Open source':
                                    open_source.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'DIY':
                                    diy.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))
                                elif x == 'Hardware':
                                    hardware.append(folio)
                                    cat_page.append((x, y, page, folio, "col2"))

                                #place_categories(folio, block_des_notes, y)
                                    #print(lines[10])
                        page = doc.addpage()

                        folio += 1

                        if folio == 21:
                            y = 6.5
                            for i in range(5):
                                x = -4
                                for i in range(7):
                                    page.place(t.ellipse(x, y, 3, 3))
                                    x += 1
                                y += 5.3

                            y += 10.3
                            for i in range(20):
                                x = -4
                                for i in range(7):
                                    page.place(t.ellipse(x, y, 3, 3))
                                    x += 1
                                y += 5.3
                            y = 112.5
                            for i in range(5):
                                x = 130
                                for i in range(12):
                                    page.place(t.ellipse(x, y, 3, 3))
                                    x += 1
                                y += 5.3

                        if folio == 22:
                            y = 49
                            for i in range(7):
                                x = -4
                                for i in range(5):
                                    page.place(t.ellipse(x, y, 3, 3))
                                    x += 1
                                y += 5.3

                            y = 38
                            for i in range(18):
                                x = 128
                                for i in range(12):
                                    page.place(t.ellipse(x, y, 3, 3))
                                    x += 1
                                y += 5.3

                            y = 23
                            for j in range(4):
                                for i in range(50):
                                    x = 128
                                    for i in range(20):
                                        page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                                        x += 2
                                y += 5.3

                        if folio == 23:
                            x = 58
                            y = 64.7
                            for i in range(7):
                                x = -4
                                for i in range(7):
                                    page.place(t.ellipse(x, y, 3, 3))
                                    x += 1
                                y += 5.3

                            y = 49
                            for i in range(4):
                                x = 130
                                for i in range(13):
                                    page.place(t.ellipse(x, y, 3, 3))
                                    x += 1
                                y += 5.3

                            y = 17
                            for j in range(3):
                                for i in range(50):
                                    x = -6
                                    for i in range(5):
                                        page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                                        x += 2
                                y += 5.3
                            y = 123.5
                            for j in range(5):
                                for i in range(50):
                                    x = -6
                                    for i in range(5):
                                        page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                                        x += 2
                                y += 5.3
                            y = 28
                            for j in range(3):
                                for k in range(50):
                                    x = 130
                                    for i in range(6):
                                        page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                                        x += 2
                                y += 5.3

                            y = 102
                            for j in range(9):
                                for k in range(50):
                                    x = 130
                                    for i in range(6):
                                        page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                                        x += 2
                                y += 5.3

                        if folio == 24:
                            y = 107
                            for i in range(5):
                                x = -4
                                for i in range(5):
                                    page.place(t.ellipse(x, y, 3, 3))
                                    x += 1
                                y += 5.3

                            y = 38.5
                            for k in range(12):
                                for i in range(50):
                                    x = -4
                                    for i in range(3):
                                        page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                                        x += 2
                                y += 5.3

                            y = 23
                            for j in range(10):
                                for i in range(50):
                                    x = 128
                                    for i in range(5):
                                        page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                                        x += 2
                                y += 5.3

                            y = 150
                            for j in range(2):
                                for i in range(50):
                                    x = 128
                                    for i in range(5):
                                        page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                                        x += 2
                                y += 5.3

                        if folio == 25:
                            y = 6
                            for j in range(3):
                                for i in range(50):
                                    x = 130
                                    for i in range(5):
                                        page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                                        x += 2
                                y += 5.3
                            y = 39
                            for j in range(3):
                                for i in range(50):
                                    x = 130
                                    for i in range(5):
                                        page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                                        x += 2
                                y += 5.3

                            y += 5.3
                            for j in range(11):
                                for k in range(50):
                                    x = 130
                                    for i in range(5):
                                        page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                                        x += 2
                                y += 5.3
                            legende1 = "Picture of the workshop"
                            legende1_txt = page.place(legende.text(legende1))
                            legende1_txt.frame(15, 42, 50, 50)

                        if folio == 31:
                            y = 48
                            for i in range(9):
                                for i in range(20):
                                    x = randint(-5, 2)
                                    y1 = y+randint(0,3)
                                    page.place(t.line(x, y1, x+randint(-3,3), y1))
                                y += 5.3

                        if folio == 32:
                            y = 20
                            for j in range(3):
                                for i in range(20):
                                    x = randint(-5, 2)
                                    y1 = y + randint(1, 5)
                                    page.place(t.circle(x, y1, 0.5))
                                y += 5.3

                            rect_y = 75
                            for j in range(10):
                                rect_x = -5
                                for j in range(10):
                                    page.place(t.line(-5, rect_y, 3, rect_y))
                                    page.place(t.rectangle(randint(rect_x, 2), rect_y+randint(-2,2), 0.6, 0.6))
                                rect_y += 5.25

                            y = 58
                            for i in range(5):
                                for i in range(20):
                                    x = randint(128, 133)
                                    y1 = y+randint(0,3)
                                    page.place(t.line(x, y1, x+randint(-3,3), y1))
                                y += 5.3

                        if folio == 33:
                            y = 21
                            for i in range(19):
                                for i in range(20):
                                    x = randint(-5, 2)
                                    y1 = y+randint(0,3)
                                    page.place(t.line(x, y1, x+randint(-3,3), y1))
                                y += 5.3


                        if folio == 59:
                            y = 22
                            for j in range(4):
                                x = -3
                                for i in range(8):
                                    page.place(t.line(x, y, x+1, y+1))
                                    page.place(t.line(x+1, y+1, x+1, y))
                                    x += 1
                                y += 5.3
                            y = 149.3
                            for i in range(2):
                                x = -6
                                for i in range(11):
                                    x1 = x+randint(-2,2)
                                    y1 = y+randint(-2,2)
                                    x2 = x1+randint(-2,2)
                                    y2 = y1+randint(-2,2)
                                    page.place(t.line(x, y, x1, y1))
                                    page.place(t.line(x1, y1, x2, y2))
                                    page.place(t.line(x2, y2, x, y))
                                    x += 1
                                y += 5.3
                            y = 6.5
                            for i in range(6):
                                x = 129
                                for i in range(11):
                                    x1 = x+randint(-2,2)
                                    y1 = y+randint(-2,2)
                                    x2 = x1+randint(-2,2)
                                    y2 = y1+randint(-2,2)
                                    page.place(t.line(x, y, x1, y1))
                                    page.place(t.line(x1, y1, x2, y2))
                                    page.place(t.line(x2, y2, x, y))
                                    x += 1
                                y += 5.3
                            y += 5.3
                            for i in range(9):
                                x = 129
                                for i in range(11):
                                    x1 = x+randint(-2,2)
                                    y1 = y+randint(-2,2)
                                    x2 = x1+randint(-2,2)
                                    y2 = y1+randint(-2,2)
                                    page.place(t.line(x, y, x1, y1))
                                    page.place(t.line(x1, y1, x2, y2))
                                    page.place(t.line(x2, y2, x, y))
                                    x += 1
                                y += 5.3

                        if folio == 60:
                            y = 6.3
                            for i in range(8):
                                x = 126
                                for i in range(11):
                                    x1 = x+randint(-2,2)
                                    y1 = y+randint(-2,2)
                                    x2 = x1+randint(-2,2)
                                    y2 = y1+randint(-2,2)
                                    page.place(t.line(x, y, x1, y1))
                                    page.place(t.line(x1, y1, x2, y2))
                                    page.place(t.line(x2, y2, x, y))
                                    x += 1
                                y += 5.3
                            y += 5.3
                            for i in range(6):
                                x = 126
                                for i in range(11):
                                    x1 = x+randint(-2,2)
                                    y1 = y+randint(-2,2)
                                    x2 = x1+randint(-2,2)
                                    y2 = y1+randint(-2,2)
                                    page.place(t.line(x, y, x1, y1))
                                    page.place(t.line(x1, y1, x2, y2))
                                    page.place(t.line(x2, y2, x, y))
                                    x += 1
                                y += 5.3
                            y += 5.3
                            for i in range(3):
                                x = 126
                                for i in range(11):
                                    x1 = x+randint(-2,2)
                                    y1 = y+randint(-2,2)
                                    x2 = x1+randint(-2,2)
                                    y2 = y1+randint(-2,2)
                                    page.place(t.line(x, y, x1, y1))
                                    page.place(t.line(x1, y1, x2, y2))
                                    page.place(t.line(x2, y2, x, y))
                                    x += 1
                                y += 5.3
                            legende1 = "Screenshots of a webpage about the game (playtime.pem.org)"
                            legende1_txt = page.place(legende.text(legende1))
                            legende1_txt.frame(12, 80, 40, 50)

                        if folio == 69:
                            y = 23.3
                            for j in range(26):
                                for i in range(52):
                                    x = -5
                                    for i in range(8):
                                        page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                                        x += 1
                                y+= 5.3
                            y = 6
                            for j in range(29):
                                for i in range(52):
                                    x = 130
                                    for i in range(8):
                                        page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                                        x += 1
                                y+= 5.3

                        if folio == 70:
                            y = 6
                            for j in range(3):
                                for i in range(52):
                                    x = -7
                                    for i in range(8):
                                        page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                                        x += 1
                                y+= 5.3
                            y += 5.3
                            for j in range(18):
                                for i in range(52):
                                    x = -7
                                    for i in range(8):
                                        page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                                        x += 1
                                y+= 5.3
                            y = 44
                            for j in range(22):
                                for i in range(52):
                                    x = 128
                                    for i in range(8):
                                        page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                                        x += 1
                                y+= 5.3

                        if folio == 71:
                            y = 6
                            for j in range(17):
                                for i in range(52):
                                    x = -5
                                    for i in range(8):
                                        page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                                        x += 1
                                y+= 5.3
                            y = 123.5
                            for j in range(7):
                                for i in range(52):
                                    x = -5
                                    for i in range(8):
                                        page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                                        x += 1
                                y+= 5.3
                            y = 6
                            for j in range(12):
                                for i in range(52):
                                    x = 130
                                    for i in range(8):
                                        page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                                        x += 1
                                y+= 5.3



                        col = 1

                    elif col == 1:

                        if folio % 2 == 0:
                            block.frame(x_pg_col1, y_bloc_standard, w_blocs, h_bloc_standard)
                            tcg = a["titreS"]
                            tcg_txt = page.place(titre_courant.text(tcg))
                            if tcg == "A Brief History of feminism and technology ":
                                tcg_txt.frame(63,165,60,30)
                            elif tcg == "Inter/de-pend-ence: the game ":
                                tcg_txt.frame(63,165,50,30)
                            elif tcg == "F12 how to use a web inspector ":
                                tcg_txt.frame(63,166,60,30)
                            elif tcg == "Animata: an uncommon free software approach to mapped anti-visuals ":
                                tcg_txt.frame(63,161,55,30)
                            elif tcg == "Create a website that cannot be hacked but looks awesome! " :
                                tcg_txt.frame(63,162,60,30)
                            else:
                                tcg_txt.frame(63,170,68,30)
                            foliotage = page.place(folios.text("%s" % folio))
                            foliotage.frame(11, 169, 20, 20)
                            lines = block.frame(x_pg_col1, y_bloc_standard, w_blocs, h_bloc_standard).lines()
                            make_index(lines, index_entries, folio)
                            make_name_index(lines, index_names, folio)
                            catego = search_categoriesG(lines)

                            #str_cat1 = '\n'.join(catego)
                            for x in catego.keys():
                                y = catego.get(x) #value
                                if x == 'Creativity/Arts':
                                    arts.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'Feminism':
                                    feminism.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'Security/Encryption':
                                    security.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'Programming':
                                    programming.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'Games':
                                    games.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'Free & Open source':
                                    open_source.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'DIY':
                                    diy.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'Hardware':
                                    hardware.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))

                                #place_categories(folio, block_des_notes, y)
                        else:

                            block.frame(x_pd_col1, y_bloc_standard, w_blocs, h_bloc_standard)
                            tcd = a["auteur"]
                            tcd_txt = page.place(titre_courant.text(tcd))
                            tcd_txt.frame(23,170,75,30)
                            foliotage = page.place(folios.text("%s" % folio))
                            foliotage.frame(88, 169, 20, 20)
                            lines = block.frame(x_pd_col1, y_bloc_standard, w_blocs, h_bloc_standard).lines()
                            make_index(lines, index_entries, folio)
                            make_name_index(lines, index_names, folio)
                            catego = search_categoriesD(lines)

                            #str_cat1 = '\n'.join(catego)
                            for x in catego.keys():
                                y = catego.get(x) #value
                                if x == 'Creativity/Arts':
                                    arts.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'Feminism':
                                    feminism.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'Security/Encryption':
                                    security.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'Programming':
                                    programming.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'Games':
                                    games.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'Free & Open source':
                                    open_source.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'DIY':
                                    diy.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))
                                elif x == 'Hardware':
                                    hardware.append(folio)
                                    cat_page.append((x, y, page, folio, "col1"))

                                #place_categories(folio, block_des_notes, y)
                        col = 0
                    block = page.chain(block)

                    #folio = folio + 1
                else: #si pas overflow bloc standard = fin 'article'

                    if folio == 14:
                        y = 132.5
                        for i in range(5):
                            for i in range(20):
                                x = randint(-2, 0)
                                y1 = y+randint(0,3)
                                page.place(t.line(x, y1, x+randint(-3,3), y1))
                            y += 5.3
                        y = 5
                        for i in range(4):
                            for i in range(20):
                                x = randint(128, 135)
                                y1 = y+randint(0,3)
                                page.place(t.line(x, y1, x+randint(-3,3), y1))
                            y += 5.3
                        legende1 = "How key exhange between two users works"
                        legende1_txt = page.place(legende.text(legende1))
                        legende1_txt.frame(82, 102, 40, 50)

                    if folio == 66:
                        y = 6.3
                        for j in range(9):
                            for i in range(52):
                                x = 128
                                for i in range(8):
                                    page.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
                                    x += 1
                            y+= 5.3
                        for j in range(5):
                            x = 125
                            for i in range(6):
                                page.place(t.line(x, y, x+1, y+1))
                                page.place(t.line(x+1, y+1, x+1, y))
                                x += 1
                            y += 5.3
                        for i in range(4):
                            x = 128
                            for i in range(13):
                                page.place(t.ellipse(x, y, 3, 3))
                                x += 1
                            y += 5.3
                        legende1 = "Screenshots of video games tests made for the project with Godot Engine"
                        legende1_txt = page.place(legende.text(legende1))
                        legende1_txt.frame(12, 130, 40, 50)

                    if folio == 74:
                        y = 23.3
                        for i in range(11):
                            x = -5
                            for i in range(6):
                                page.place(t.ellipse(x, y, 3, 3))
                                x += 1
                            y += 5.3
                        y=94
                        for j in range(12):
                            for i in range(20):
                                x = randint(-5,2)
                                y1 = y + randint(1, 5)
                                page.place(t.circle(x, y1, 0.5))
                            y += 5.3
                        y=4.5
                        for j in range(3):
                            for i in range(20):
                                x = randint(126,135)
                                y1 = y + randint(1, 5)
                                page.place(t.circle(x, y1, 0.5))
                            y += 5.3
                        y += 5.3
                        for j in range(8):
                            for i in range(20):
                                x = randint(126,135)
                                y1 = y + randint(1, 5)
                                page.place(t.circle(x, y1, 0.5))
                            y += 5.3
                        y += 5.3
                        for j in range(10):
                            for i in range(20):
                                x = randint(126,135)
                                y1 = y + randint(1, 5)
                                page.place(t.circle(x, y1, 0.5))
                            y += 5.3

                    if folio == 77:
                        y=51.5
                        for j in range(4):
                            for i in range(20):
                                x = randint(-5,4)
                                y1 = y + randint(1, 5)
                                page.place(t.circle(x, y1, 0.5))
                            y += 5.3
                        y=4
                        for j in range(7):
                            for i in range(20):
                                x = randint(128,135)
                                y1 = y + randint(1, 5)
                                page.place(t.circle(x, y1, 0.5))
                            y += 5.3




                    if folio % 2 == 0:
                        block.frame(x_pg_col1, y_bloc_standard, w_blocs, h_bloc_standard)
                        tcg = a['titreS']
                        tcg_txt = page.place(titre_courant.text(tcg))
                        if tcg == "Elements for a feminist video game Godot Engine ":
                            tcg_txt.frame(63,165,60,30)
                        elif tcg == "Inter/de-pend-ence: the game ":
                            tcg_txt.frame(63,165,50,30)
                        elif tcg == "F12 how to use a web inspector ":
                            tcg_txt.frame(63,184,60,30)
                        elif tcg == "Animata: an uncommon free software approach to mapped anti-visuals ":
                            tcg_txt.frame(63,185,60,30)
                        elif tcg == "Create a website that cannot be hacked but looks awesome! " :
                            tcg_txt.frame(63,162,60,30)
                        else:
                            tcg_txt.frame(63,170,68,30)
                        foliotage = page.place(folios.text("%s" % folio))
                        foliotage.frame(11, 169, 20, 20)
                        lines = block.frame(x_pg_col1, y_bloc_standard, w_blocs, h_bloc_standard).lines()
                        make_index(lines, index_entries, folio)
                        make_name_index(lines, index_names, folio)
                        catego = search_categoriesG(lines)
                        #str_cat1 = '\n'.join(catego)
                        for x in catego.keys():
                            y = catego.get(x) #value
                            if x == 'Creativity/Arts':
                                arts.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'Feminism':
                                feminism.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'Security/Encryption':
                                security.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'Programming':
                                programming.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'Games':
                                games.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'Free & Open source':
                                open_source.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'DIY':
                                diy.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'Hardware':
                                hardware.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))

                            #place_categories(folio, block_des_notes, y)

                        if 'imgIn' in a:
                            imgin = image.open(a.get('imgIn'))
                            block_imgin = page.place(imgin)
                            if folio == 10:
                                block_imgin.frame(-3,5,62,100)
                                block_imgin.fitwidth(62)
                            elif folio == 14:
                                block_imgin.frame(70,32,60,500)
                                block_imgin.fitwidth(60)
                            elif folio == 33:
                                block_imgin.frame(72,50,60,160)
                                block_imgin.fitwidth(60)
                            elif folio == 60:
                                block_imgin.frame(-3,5,65,160)
                                block_imgin.fitwidth(62)
                            else:
                                block_imgin.frame(-5,5,65,500)
                                block_imgin.fitwidth(65)

                        if 'imgFin' in a:
                            page = doc.addpage()
                            folio += 1
                            imgFin = image.open(a.get('imgFin'))
                            block_imgfin = page.place(imgFin)
                            block_imgfin.frame(-1,-7.5,132,160)
                            block_imgfin.fitwidth(132)

                        if block.frame(x_pg_col2, y_bloc_standard, w_blocs, h_bloc_standard).overflow():
                            block = page.chain(block)
                            block.frame(x_pg_col2, y_bloc_standard, w_blocs, h_bloc_standard)
                    else:

                        block.frame(x_pd_col2, y_bloc_standard, w_blocs, h_bloc_standard)
                        tcd = a["auteur"]
                        tcd_txt = page.place(titre_courant.text(tcd))
                        tcd_txt.frame(23,170,75,30)
                        foliotage = page.place(folios.text("%s" % folio))
                        foliotage.frame(88, 169, 20, 20)
                        lines = block.frame(x_pd_col2, y_bloc_standard, w_blocs, h_bloc_standard).lines()
                        make_index(lines, index_entries, folio)
                        make_name_index(lines, index_names, folio)
                        catego = search_categoriesD(lines)

                        #str_cat1 = '\n'.join(catego)
                        for x in catego.keys():
                            y = catego.get(x) #value
                            if x == 'Creativity/Arts':
                                arts.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'Feminism':
                                feminism.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'Security/Encryption':
                                security.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'Programming':
                                programming.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'Games':
                                games.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'Free & Open source':
                                open_source.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'DIY':
                                diy.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))
                            elif x == 'Hardware':
                                hardware.append(folio)
                                cat_page.append((x, y, page, folio, "col2"))

                        if 'imgIn' in a:
                            imgin = image.open(a.get('imgIn'))
                            block_imgin = page.place(imgin)
                            if folio == 33:
                                block_imgin.frame(72,76,60,160)
                                block_imgin.fitwidth(60)
                                legende1 = "Screenshot of Qubes OS"
                                legende1_txt = page.place(legende.text(legende1))
                                legende1_txt.frame(84, 112, 40, 50)
                                legende2 = "Screenshot of Tails OS"
                                legende2_txt = page.place(legende.text(legende2))
                                legende2_txt.frame(84, 154, 40, 50)
                            else:
                                block_imgin.frame(-3,5,65,160)
                                block_imgin.fitwidth(65)

                        if 'imgFin' in a:
                            page = doc.addpage()
                            folio += 1
                            imgFin = image.open(a.get('imgFin'))
                            block_imgfin = page.place(imgFin)
                            block_imgfin.frame(0,5,65,160)
                            block_imgfin.fitwidth(65)
#value
                            #place_categories(folio, block_des_notes, y)

                        if block.frame(x_pd_col2, y_bloc_standard, w_blocs, h_bloc_standard).overflow():
                            block = page.chain(block)
                            block.frame(x_pd_col2, y_bloc_standard, w_blocs, h_bloc_standard)

            #else:
                #block.frame(10, 90, 85, 60)





        folio += 1 #print(width(pieces[2]))

        pages.append(folio)

    post_pages = []

    for i in range(9, 78):
        post_pages.append([i, "page", {}])



    arts_list = [' ']
    diy_list = [' ']
    hardware_list = [' ']
    feminism_list = [' ']
    games_list = [' ']
    os_list = [' ']
    programming_list = [' ']
    security_list = [' ']



    for c in cat_page:
        if c[0] == "Creativity/Arts":
            if c[3] not in arts_list:
                arts_list.append(c[3])
        elif c[0] == "DIY":
            if c[3] not in diy_list:
                diy_list.append(c[3])
        elif c[0] == "Feminism":
            if c[3] not in feminism_list:
                feminism_list.append(c[3])
        elif c[0] == "Games":
            if c[3] not in games_list:
                games_list.append(c[3])
        elif c[0] == "Hardware":
            if c[3] not in hardware_list:
                hardware_list.append(c[3])
        elif c[0] == "Free & Open source":
            if c[3] not in os_list:
                os_list.append(c[3])
        elif c[0] == "Programming":
            if c[3] not in programming_list:
                programming_list.append(c[3])
        elif c[0] == "Security/Encryption":
            if c[3] not in security_list:
                security_list.append(c[3])

    arts_list.append(' ')
    diy_list.append(' ')
    feminism_list.append(' ')
    games_list.append(' ')
    hardware_list.append(' ')
    os_list.append(' ')
    programming_list.append(' ')
    security_list.append(' ')


    categoriesL = [
        ("Creativity/Arts", arts_list),
        ("DIY", diy_list),
        ("Feminism", feminism_list),
        ("Games", games_list),
        ("Hardware", hardware_list),
        ("Free & Open source", os_list),
        ("Programming", programming_list),
        ("Security/Encryption", security_list)
        ]



    for p in cat_page:
        for i in range(len(post_pages)):
            n_folio = 0
            if p[3] == post_pages[i][0]:
                post_pages[i][1] = p[2]
                #print(post_pages)
                if p[0] not in post_pages[i][2]:
                    post_pages[i][2][p[0]] = [p[0]]
                    #print(post_pages[i][2][p[0]])

                    for c in categoriesL:
                        if c[0] == str(post_pages[i][2][p[0]][0]):
                            #print(post_pages[i][2][p[0]])
                            post_pages[i][2][p[0]].append(c[1][n_folio])
                            post_pages[i][2][p[0]].append(c[1][n_folio+2])
                            post_pages[i][2][p[0]].append(p[1])
                            post_pages[i][2][p[0]].append(p[4])
                            c[1].pop(0)




    for p in post_pages:
        categos = list(p[2].keys())

        if p[1] != 'page':
            if len(list(p[2].keys())) == 1:
                note1 = p[1].place(note.text(categos[0]))
                if p[0] % 2 == 0:
                    block_note1 = note1.frame(22, 169, 60, 30)
                else:
                    block_note1 = note1.frame(99, 169, 60, 30)
            elif len(list(p[2].keys())) == 3:
                note1 = p[1].place(note.text(categos[0]))
                note2 = p[1].place(note.text(categos[1]))
                note3 = p[1].place(note.text(categos[2]))
                if p[0] % 2 == 0:
                    block_note1 = note1.frame(22, 166.5, 60, 30)
                    block_note2 = note2.frame(22, 169.5, 60, 30)
                    block_note3 = note3.frame(22, 172.5, 60, 30)
                else:
                    block_note1 = note1.frame(99, 166.5, 60, 30)
                    block_note2 = note2.frame(99, 169.5, 60, 30)
                    block_note3 = note3.frame(99, 172.5, 60, 30)
            else:
                note1 = p[1].place(note.text(categos[0]))
                note2 = p[1].place(note.text(categos[1]))
                if p[0] % 2 == 0:
                    block_note1 = note1.frame(22, 169.5, 60, 30)
                    block_note2 = note2.frame(22, 172.5, 60, 30)
                else:
                    block_note1 = note1.frame(99, 169.5, 60, 30)
                    block_note2 = note2.frame(99, 172.5, 60, 30)

            pages_refs = list(p[2].values())

            for i in range(len(pages_refs)):
                if pages_refs[i][1] == ' ':
                    ref_1 = "%s" %str(pages_refs[i][1])
                else:
                    ref_1 = "{%s" %str(pages_refs[i][1])
                ref1 = p[1].place(note.text(ref_1))
                if pages_refs[i][2] == ' ':
                    ref_2 = "%s" %str(pages_refs[i][2])
                else:
                    ref_2 = "%s}" %str(pages_refs[i][2])
                ref2 = p[1].place(note.text(ref_2))
                ligne = pages_refs[i][3]
                colonne = pages_refs[i][4]

                place_categories(p[0], ref1, ref2, ligne, colonne)


    note_to_place1 = "Arts"
    note_to_place2 = "DIY"
    note_to_place3 = "Feminism"
    note_to_place4 = "Games"
    note_to_place5 = "Hardware"
    note_to_place6 = "Open source"
    note_to_place7 = "Programming"
    note_to_place8 = "Security"

    index_cat1 =  "%s \n \n \n \n %s \n  \n\n \n %s \n \n \n \n %s \n \n \n \n %s \n \n \n \n %s \n \n \n \n %s \n \n \n \n %s" %(note_to_place1, note_to_place2, note_to_place3, note_to_place4, note_to_place5, note_to_place6, note_to_place7, note_to_place8)
    block_note1 = p_sommaire2.place(body.text(index_cat1))
    block_note1.frame(98, 5, 50, 160)


    warn =  "The content of the workshops could be transcripted thanks to audio records, pads and personal notes. \n\nThe 2018 edition of the Eclectic Tech Carnival in Bologna gathered workshops and talks about 8 main themes: arts, DIY, feminism, games, hardware, open source, programming and security. \nThrough the book, when some-thing is about one of these themes, it is marked in the margins of the page with the following graphic signs (one per theme) and pages references for the previous and the next occurrence of the theme. \nThe idea of this system was to show the richness of the event and how themes crossed eachother all along the workshops."
    warn_block = p_sommaire2.place(body.text(warn))
    warn_block.frame(15, 5, 55, 150)
    y = 5
    for i in range(3):
        x = 129
        for i in range(3):
            x1 = x+randint(-2,2)
            y1 = y+randint(-2,2)
            x2 = x1+randint(-2,2)
            y2 = y1+randint(-2,2)
            p_sommaire2.place(t.line(x, y, x1, y1))
            p_sommaire2.place(t.line(x1, y1, x2, y2))
            p_sommaire2.place(t.line(x2, y2, x, y))
            x += 3
        y += 5.3
    y = 5
    for i in range(3):
        x = 88
        for i in range(2):
            x1 = x+randint(-2,2)
            y1 = y+randint(-2,2)
            x2 = x1+randint(-2,2)
            y2 = y1+randint(-2,2)
            p_sommaire2.place(t.line(x, y, x1, y1))
            p_sommaire2.place(t.line(x1, y1, x2, y2))
            p_sommaire2.place(t.line(x2, y2, x, y))
            x += 3
        y += 5.3
    y = 26
    for j in range(2):
        x = 127
        for i in range(2):
            p_sommaire2.place(t.line(x, y, x+5, y+3))
            p_sommaire2.place(t.line(x+5, y, x, y+3))
            x += 3
        y += 5.3
    y = 26
    for j in range(2):
        x = 86
        for i in range(2):
            p_sommaire2.place(t.line(x, y, x+5, y+3))
            p_sommaire2.place(t.line(x+5, y, x, y+3))
            x += 3
        y += 5.3
    y = 49
    for j in range(2):
        x = 130
        for k in range(4):
            for i in range(52):
                p_sommaire2.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
            x += 1
        y += 5.3
    y = 49
    for j in range(2):
        x = 90
        for k in range(2):
            for i in range(52):
                p_sommaire2.place(t.line(x, y, x+randint(-3,3), y+randint(-3,3)))
            x += 1
        y += 5.3
    y = 70
    for j in range(2):
        x = 127
        for i in range(8):
            p_sommaire2.place(t.line(x, y, x+1, y+1))
            p_sommaire2.place(t.line(x+1, y+1, x+1, y))
            x += 1
        y += 5.3
    y = 70
    for j in range(2):
        x = 88
        for i in range(6):
            p_sommaire2.place(t.line(x, y, x+1, y+1))
            p_sommaire2.place(t.line(x+1, y+1, x+1, y))
            x += 1
        y += 5.3
    rect_y = 90
    for i in range(2):
        rect_x = 127
        for j in range(8):
            p_sommaire2.place(t.line(127, rect_y, 135, rect_y))
            p_sommaire2.place(t.rectangle(rect_x, rect_y+randint(-2,2), 0.6, 0.6))
            rect_x += randint(0,2)
        rect_y += 5.25
    rect_y = 90
    for i in range(2):
        rect_x = 88
        for j in range(5):
            p_sommaire2.place(t.line(88, rect_y, 93, rect_y))
            p_sommaire2.place(t.rectangle(rect_x, rect_y+randint(-2,2), 0.6, 0.6))
            rect_x += randint(0,2)
        rect_y += 5.25
    y = 113
    for j in range(2):
        x = 130
        for i in range(5):
            p_sommaire2.place(t.ellipse(x, y, 3, 3))
            x += 1
        y += 5.3
    y = 113
    for j in range(2):
        x = 89
        for i in range(4):
            p_sommaire2.place(t.ellipse(x, y, 3, 3))
            x += 1
        y += 5.3
    y1 = 132
    for j in range(2):
        for i in range(20):
            x = randint(128, 135)
            y = y1+randint(0,3)
            p_sommaire2.place(t.circle(x, y, 0.5))
        y1+= 5.3
    y1 = 132
    for j in range(2):
        for i in range(10):
            x = randint(88, 92)
            y = y1+randint(0,3)
            p_sommaire2.place(t.circle(x, y, 0.5))
        y1+= 5.3
    y = 153
    for i in range(2):
        for i in range(40):
            x = randint(130, 135)
            y1 = y+randint(0,3)
            p_sommaire2.place(t.line(x, y1, x+randint(-3,3), y1))
        y += 5.3
    y = 153
    for i in range(2):
        for i in range(20):
            x = randint(89, 91)
            y1 = y+randint(0,3)
            p_sommaire2.place(t.line(x, y1, x+randint(-3,3), y1))
        y += 5.3



    titres1 = [
        'Hardware crash course',
        'Tunnel up, tunnel down',
        'You are okay.',
        'Open source sustainability',
        'A brief history of Linux',
        'Workshop on Immigration',
        'Ricette dal caos * assorbenti DIY',
        'Workshop on Consent'
    ]
    titres2 = [
        'Imaginary friends',
        'Introducing the Soleus VPS collective',
        'Build and reach the Internet with an antenna from the roof',
        'From data to dating',
        'Create a website that cannot be hacked but looks awesome!',
        'Dock, dock, docker',
        'Inter/de-pend-ence: the game',
        'Presentazione rivista Aspirina',
        'F12 how to use a web inspector'
    ]
    titres3 = [
        'React workshop',
        'Elements for a feminist video game Godot Engine',
        'A Brief History of feminism and technology',
        'Sex work online',
        'Animata: an uncommon free software approach to mapped anti-visuals',
        'Git explained with toys',
        'Index of terms',
        'Index of names'
    ]
    auteurs1 = ['Donna', 'Mara Elenis Daughter', 'Ulrike', 'Helen', 'Aileen', 'Milena', 'Lara e Kate', 'Mujeres Libres Bologna']
    auteurs2 = ['Obaz', 'Donna', '\nIgnifugo', 'Frocetaria', '\nObaz', 'Ivana', 'Sarrita', '', 'Ignifugo']
    auteurs3 = ['Andrea', '\nNatacha', '\nZeyev', 'Tiz', '\n', 'Ignifugo + Obaz', "", ""]
    debuts = []

    for a in articles:
        debuts.append(a['p_debut'])


    pages_debuts = []

    for p in debuts:
        ps = str(p)
        p_new = '%s' %ps
        pages_debuts.append(p_new)
    pages_debuts.append('80')
    pages_debuts.append('85')


    debuts_str = '\n \n \n'.join(x for x in pages_debuts)

    y = 25
    j = 0
    for i in titres1:
        x = randint(22, 50)
        block_index1 = p_sommaire1.place(index_titre.text(titres1[j]))
        block_index1.frame(x, y, 65, 150)
        block_index1_lines = block_index1.frame(x, y, 65, 150).lines()
        block_index2 = p_sommaire1.place(index_auteurs.text(auteurs1[j]))
        block_index2.frame(x + 10, y+4.5, 65, 150)
        block_index3 = p_sommaire1.place(index_titre.text(pages_debuts[j]))
        block_index3.frame(x - 10, y + 2, 65, 150)
        j += 1
        y += 18

    y = 8
    j = 0
    k = 8
    for i in titres2:
        x = randint(25, 53)
        block_index1 = p_sommaire.place(index_titre.text(titres2[j]))
        block_index1.frame(x, y, 65, 150)
        block_index1_lines = block_index1.frame(x, y, 65, 150).lines()
        print(block_index1_lines)
        block_index2 = p_sommaire.place(index_auteurs.text(auteurs2[j]))
        block_index2.frame(x + 10, y+4.5, 65, 150)
        block_index3 = p_sommaire.place(index_titre.text(pages_debuts[k]))
        block_index3.frame(x - 10, y+2, 65, 150)
        j += 1
        if len(block_index1_lines) == 2:
            y += 23
        else:
            y += 18
        k += 1

    y = 8
    j = 0
    k = 17
    for i in titres3:
        x = randint(20, 48)
        block_index1 = p_inter.place(index_titre.text(titres3[j]))
        block_index1.frame(x, y, 65, 150)
        block_index1_lines = block_index1.frame(x, y, 65, 150).lines()
        print(block_index1_lines)
        block_index2 = p_inter.place(index_auteurs.text(auteurs3[j]))
        block_index2.frame(x + 10, y+4.5, 65, 150)
        block_index3 = p_inter.place(index_titre.text(pages_debuts[k]))
        block_index3.frame(x - 10, y+2, 65, 150)
        j += 1
        if len(block_index1_lines) == 2 and pages_debuts[k] != "73":
            y += 23
        else:
            y += 18
        k += 1

    p_img = doc.addpage()
    xm24 = image.open('xm24.jpg')
    xm = p_img.place(xm24)
    xm.frame(-35,-5,305,160)
    xm.fitwidth(305)
    folio += 1

    p_img = doc.addpage()
    xm24 = image.open('xm24.jpg')
    xm = p_img.place(xm24)
    xm.frame(-165,-5,305,160)
    xm.fitwidth(305)

    p_index = doc.addpage()
    folio += 1

    index_final = []
    for key, value in index_entries.items():
        pages = ', '.join(str(x) for x in value)
        if key == 'browser':
            entree = "\n\n %s \n%s" %(key, pages)
            index_final.append(entree)
        elif key == 'computer':
            entree = "\n %s \n%s" %(key, pages)
            index_final.append(entree)
        elif key == 'connection':
            entree = " %s \n%s" %(key, pages)
            index_final.append(entree)
        elif key == '/ETC':
            entree = " %s \n%s" %(key, pages)
            index_final.append(entree)
        elif key == 'feminine':
            entree = " %s \n%s" %(key, pages)
            index_final.append(entree)
        elif key == 'gender':
            entree = "\n %s \n%s" %(key, pages)
            index_final.append(entree)
        elif key == 'hash':
            entree = "\n\n %s \n%s" %(key, pages)
            index_final.append(entree)
        elif key == 'IPSec':
            entree = "\n\n\n %s \n%s" %(key, pages)
            index_final.append(entree)
        elif key == 'Linux':
            entree = "\n %s \n%s" %(key, pages)
            index_final.append(entree)
        elif key == 'man':
            entree = " %s \n%s" %(key, pages)
            index_final.append(entree)
        elif key == 'network':
            entree = "\n\n %s \n%s" %(key, pages)
            index_final.append(entree)
        elif key == 'privacy':
            entree = "\n\n %s \n%s" %(key, pages)
            index_final.append(entree)
        elif key == 'secure':
            entree = "\n\n %s \n%s" %(key, pages)
            index_final.append(entree)
        elif key == 'society':
            entree = "\n\n %s \n%s" %(key, pages)
            index_final.append(entree)
        elif key == 'tunnel':
            entree = "\n\n\n\n %s \n%s" %(key, pages)
            index_final.append(entree)
        elif key == 'Windows':
            entree = "\n\n %s \n%s" %(key, pages)
            index_final.append(entree)
        else:
            entree = "\n %s \n%s" %(key, pages)
            index_final.append(entree)




    index_str = "\n".join(x for x in index_final)

    index_bloc = p_index.place(body.text(index_str))
    ind = 'index of terms'

    cols = 1

    index_bloc.frame(89, 25, 26, 150)

    while index_bloc.frame(89, 25, 26, 150).overflow():
        if cols == 0:
            index_bloc.frame(89, 25, 26, 150)
            if folio % 2 == 0:
                index_bloc.frame(87, 25, 26, 150)
                ind_txt = p_index.place(sommaire.text(ind))
                ind_txt.frame(18,5,80,20)
                foliotage = p_index.place(folios.text("%s" % folio))
                foliotage.frame(11, 169, 20, 20)
            else:
                index_bloc.frame(89, 25, 26, 150)
                ind_txt = p_index.place(sommaire.text(ind))
                ind_txt.frame(72,5,80,20)
                foliotage = p_index.place(folios.text("%s" % folio))
                foliotage.frame(88, 169, 20, 20)
            p_index = doc.addpage()
            folio += 1
            cols = 1
        elif cols == 1:
            index_bloc.frame(10, 25, 26, 150)
            if folio % 2 == 0:
                index_bloc.frame(8, 25, 26, 150)
            else:
                index_bloc.frame(10, 25, 26, 150)
            cols = 2
        elif cols == 2:
            index_bloc.frame(49, 25, 26, 148)
            if folio % 2 == 0:
                index_bloc.frame(47, 25, 26, 150)
            else:
                index_bloc.frame(49, 25, 26, 150)
            cols = 0
        index_bloc = p_index.chain(index_bloc)
    else:
        index_bloc.frame(87, 25, 26, 150)
        ind_txt = p_index.place(sommaire.text(ind))
        ind_txt.frame(18,5,80,20)
        foliotage = p_index.place(folios.text("%s" % folio))
        foliotage.frame(11, 169, 20, 20)


    p_index_noms = doc.addpage()
    folio += 1

    index_noms_final = []
    for key, value in index_names.items():
        pages = ', '.join(str(x) for x in value)
        if key == 'de Beauvoir, Simone':
            entree = "%s \n%s" %(key, pages)
            index_noms_final.append(entree)
        elif key == 'Krasmogonowi, Hilde':
            entree = "\n\n%s \n%s" %(key, pages)
            index_noms_final.append(entree)
        elif key == 'Snowden, Edward':
            entree = "\n\n\n%s \n%s" %(key, pages)
            index_noms_final.append(entree)
        else:
            entree = "\n %s \n%s" %(key, pages)
            index_noms_final.append(entree)


    index_noms_str = "\n".join(x for x in index_noms_final)
    print(index_noms_str)

    index_noms_bloc = p_index_noms.place(body.text(index_noms_str))
    ind_noms = 'index of names'

    cols = 1

    index_noms_bloc.frame(87, 25, 26, 100)

    while index_noms_bloc.frame(87, 25, 26, 100).overflow():
        if cols == 0:
            index_noms_bloc.frame(87, 25, 26, 100)
            if folio % 2 == 0:
                index_noms_bloc.frame(89, 25, 26, 100)
                ind_noms_txt = p_index_noms.place(sommaire.text(ind_noms))
                ind_noms_txt.frame(72,5,80,20)

            else:
                index_noms_bloc.frame(87, 25, 26, 100)
                ind_noms_txt = p_index_noms.place(sommaire.text(ind_noms))
                ind_noms_txt.frame(18,5,80,20)

            p_index_noms = doc.addpage()
            folio += 1
            cols = 1
        elif cols == 1:
            index_noms_bloc.frame(8, 25, 26, 100)
            if folio % 2 == 0:
                index_noms_bloc.frame(10, 25, 26, 100)
            else:
                index_noms_bloc.frame(8, 25, 26, 100)
            cols = 2
        elif cols == 2:
            index_noms_bloc.frame(47, 25, 26, 100)
            if folio % 2 == 0:
                index_noms_bloc.frame(49, 25, 26, 100)
            else:
                index_noms_bloc.frame(47, 25, 26, 100)
            cols = 0
        index_noms_bloc = p_index_noms.chain(index_noms_bloc)
    else:
        index_noms_bloc.frame(87, 25, 26, 100)
        ind_noms_txt = p_index_noms.place(sommaire.text(ind_noms))
        ind_noms_txt.frame(72,5,80,20)
        foliotage = p_index_noms.place(folios.text("%s" % folio))
        foliotage.frame(88, 169, 20, 20)

    p_colophon = doc.addpage()
    folio += 1
    coloph = "colophon"
    colophon_txt = p_colophon.place(sommaire.text(coloph))
    colophon_txt.frame(18,5,80,20)
    coloph_texte = "This book was laidout in Python (Flat) by Amélie Dumont \n\nTypefaces: Redaction 35, Cirrus Cumulus and Lab Mono"
    colophon_texte = p_colophon.place(body.text(coloph_texte))
    colophon_texte.frame(8,25,35,120)
    coloph_texte2 = "  Credits of the images \n\nCover: artwork designed for /ETC 2018 in Bologna \n\nPages 8 and 10: pictures shot during the workshop 'Hardware Crash Course' \n\nPage 14: image by David Göthberg, commons.wikimedia.org \n\nPage 18: screenshots of Upstage website (upstage.org.nz) \n\nPage 25: picture shot during the workshop 'A Brief History of Linux' \n\nPage 33: Qubes OS screenshot by Brunogabuzomeu, commons. wikimedia.org. Tails OS screenshot by Les Pounder, commons.wikimedia.org \n\nPage 39: screenshot of Ninux website (map.ninux.org)\n\n Page 49: screenshot of the /ETC website (eclectictechcarnival.org)"
    colophon_texte2 = p_colophon.place(body.text(coloph_texte2))
    colophon_texte2.frame(52,25,65,150)

    p_colophon2 = doc.addpage()
    folio += 1
    colophon_txt2 = p_colophon2.place(sommaire.text(coloph))
    colophon_txt2.frame(80,5,80,20)
    coloph_texte3 = "Page 51: image by Maklaan, commons.wikimedia.org \n\nPage 53: screenshots of review Aspirina on the website (erbaccelarivista.org) \n\nPage 54: picture shot during the workshop 'Inter/de-pend-ence: the game' \n\nPage 60: screenshots of an article about the game Inter/de-pend-ence on playtime.pem.org \n\nPage 63: screenshot of the /ETC website inspected in Chromium \n\nPage 66: screenshots of video game tests for Jeu-videa project \n\nPages 78 and 79: picture shot in XM24 during the event"
    colophon_texte3 = p_colophon2.place(body.text(coloph_texte3))
    colophon_texte3.frame(15,25,65,150)

    return doc



doc = layout()

doc.pdf('etc2018_doc.pdf',compress=False, bleed=False, cropmarks=False)

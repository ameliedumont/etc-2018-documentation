from flat import *
from random import *
import codecs

def layout():
    purple = rgb(204, 0, 255)
    green = cmyk(255, 0, 120, 55)
    white = rgb(255, 255, 255)

    fonte = font.open('Redaction35-Regular.ttf')
    fonte_1 = font.open('CirrusCumulus.otf')
    fonte_folio = font.open('LabMono-Regular.otf')
    fonte_notes = font.open('Redaction35-Regular.ttf')

    white = cmyk(0, 0, 0, 0)
    black1 = cmyk(0, 0, 0, 255)
    black = overprint(black1)

    name = strike(fonte_notes).size(20, 40)
    body = strike(fonte_notes).size(10, 15)
    headline = strike(fonte_1).size(44, 44)
    folios = strike(fonte_folio).size(20, 10)
    note = strike(fonte_folio).size(5.5, 14).color(green)
    legende = strike(fonte_folio).size(7, 10)
    index_note = strike(fonte_notes).size(9, 14)
    index_titre = strike(fonte_notes).size(11, 12)
    index_auteurs = strike(fonte_notes).size(11, 12)
    sommaire = strike(fonte_1).size(18, 10)
    titre_courant = strike(fonte_1).size(14, 14)


    #t = shape().stroke(black).width(1)

    doc = document(270, 180, 'mm')

    p = doc.addpage()
    accueil = "Eclectic Tech Carnival 2018 \n@ XM24 \nBologna, Italy"
    accueil_style = strike(fonte_1).size(20, 23).color(green)
    accueil_txt = p.place(accueil_style.text(accueil))
    accueil_txt.frame(12, 5, 50, 153)

    dostxt = "/ \nE T C \n\n2 0 \n1 \n8 \n\n@ \n\nX M 2 \n4"
    dos_style = strike(fonte).size(18, 25).color(purple)
    dos_txt = p.place(dos_style.text(dostxt))
    dos_txt.frame(132.5, 20, 8, 173)




    xm24 = image.open('cyborg_cover.jpg')
    xm = p.place(xm24)
    xm.frame(140,-3,130,160)
    xm.fitwidth(130)

    front1 = "/ETC"
    front2 = "2018"
    front3 = "XM24"
    front4 = "Bologna"
    front_style = strike(fonte_1).size(22, 23).color(green)
    front1_txt = p.place(front_style.text(front1))
    front1_txt.frame(147, 6, 50, 53)
    front2_txt = p.place(front_style.text(front2))
    front2_txt.frame(246, 6, 50, 53)
    front3_txt = p.place(front_style.text(front3))
    front3_txt.frame(148, 168, 50, 53)
    front4_txt = p.place(front_style.text(front4))
    front4_txt.frame(237, 168, 50, 53)


    intro = "The Eclectic Tech Carnival (/ETC) is a gathering of people who critically use/study/share/ improve everyday information technologies in the context of the free speech and open hardware movements. \nWe are a collective body of feminists with a particular history, who chew on the roots \nof control and domination. \nEvents are organised locally and autonomously where there is a desire to host an /ETC. \nEveryone participates in skill sharing to expand our knowledges."

    introd = strike(fonte).size(12, 16).color(green)
    intro_txt = p.place(introd.text(intro))
    intro_txt.frame(12, 70, 60, 153)


    f = shape().fill(black)
    t = shape().stroke(green).width(0.1, units='pt')
    t2 = shape().stroke(purple).width(0.1, units='pt')


    y = -2
    for i in range(8):
        x = 70
        for i in range(14):
            x1 = x+randint(-2,2)
            y1 = y+randint(-2,2)
            x2 = x1+randint(-2,2)
            y2 = y1+randint(-2,2)
            p.place(t2.line(x, y, x1, y1))
            p.place(t2.line(x1, y1, x2, y2))
            p.place(t2.line(x2, y2, x, y))
            x += 3
        y += 5.3
    y = 174
    for j in range(3):
        x = 11
        for i in range(20):
            p.place(t2.line(x, y, x+5, y+3))
            p.place(t2.line(x+5, y, x, y+3))
            x += 3
        y += 3


    y = -4
    for j in range(40):
        x = -5
        for k in range(5):
            for i in range(52):
                p.place(t2.line(x, y, x+randint(-3,3), y+randint(-3,3)))
            x += 1
        y += 5.3
    y = 44
    for j in range(4):
        x = 12
        for i in range(60):
            p.place(t2.line(x, y, x+1, y+1))
            p.place(t2.line(x+1, y+1, x+1, y))
            x += 1
        y += 5.3

    y = 47
    for j in range(28):
        x = 83
        for i in range(37):
            p.place(t2.ellipse(x, y, 3, 3))
            x += 1
        y += 5.3

    y = 163
    for i in range(5):
        for i in range(40):
            x = randint(120, 131)
            y1 = y+randint(0,3)
            p.place(t2.line(x, y1, x+randint(-3,3), y1))
        y += 5.3
    y = -3
    for i in range(4):
        for i in range(40):
            x = randint(120, 131)
            y1 = y+randint(0,3)
            p.place(t2.line(x, y1, x+randint(-3,3), y1))
        y += 5.3




    return doc



doc = layout()

doc.pdf('etc2018_cover.pdf',compress=False, bleed=False, cropmarks=True)
